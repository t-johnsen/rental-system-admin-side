package test;

import data.Coordinate;
import data.DockingStation;
import db.DockingStationDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class DockingDaoTest{
	
	DockingStationDAO dao;
	DockingStation dock;
	
	@Before
	public void setup(){
		dao = new DockingStationDAO();
		dock = new DockingStation("SomeName", new Coordinate(10.14353923, 63.23412154));
	}
	
	@Test
	public void testAddDock(){
		//Becomes 1 only when first test. Later by new ID numbers.
		int expResult = 1;
		int result = dao.addDockingStation(dock);
		Assert.assertEquals(expResult, result);
	}
	
	@Test
	public void testEditDockingstation(){
		int expResult = dock.getDockingID();
		int result = dao.editDockingStation(expResult, "NewName", 10.243535689, 63.2341098);
		Assert.assertEquals(expResult, result);
	}
	
}
