package test;

import data.Type;
import db.TypeDAO;
import org.junit.Assert;
import org.junit.Test;


public class TypeDAOTest {
    TypeDAO typeDAO = new TypeDAO();

    @Test
    public void testRegister()
    {
        Type type = new Type();
        type.setType("TestType");
        type.setPrice(100);
        int id = typeDAO.addType(type);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);

        Type newType = typeDAO.getType(id);

        Assert.assertEquals("TestType",newType.getType());
        Assert.assertEquals(100,newType.getPrice());

        return;
    }

    @Test
    public void testeditPrice(){
        Type type = new Type();
        type.setType("TestPriceType");
        type.setPrice(150);
        int id = typeDAO.addType(type);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);
        typeDAO.editPrice(id, 200);

        Type newType = typeDAO.getType(id);
        Assert.assertEquals(200,newType.getPrice());
        return;
    }
}
