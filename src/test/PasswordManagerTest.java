package test;

import org.junit.Assert;
import org.junit.Test;
import util.PasswordManager;

public class PasswordManagerTest{

	
	@Test
	public void testHashing(){
		String pw1 = "onePassword";
		String pw2 = "anotherPassword";
		
		
		String hash1 = PasswordManager.hashPassword(pw1);
		Assert.assertTrue(PasswordManager.validatePassword(pw1, hash1));
		
		String hash2 = PasswordManager.hashPassword(pw2);
		Assert.assertTrue(PasswordManager.validatePassword(pw2, hash2));
		
		Assert.assertFalse(PasswordManager.validatePassword(pw1, hash2));
		Assert.assertFalse(PasswordManager.validatePassword(pw2, hash1));
	}
	
	@Test
	public void testValidatePassword(){
		
		String tempPassword = PasswordManager.generateTempPassword();
		System.out.println(tempPassword);
		String hash1 = PasswordManager.hashPassword(tempPassword);
		System.out.println(hash1);
		Assert.assertTrue(PasswordManager.validatePassword(tempPassword, hash1));
	}


}
