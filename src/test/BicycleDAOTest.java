package test;

import data.Bicycle;
import data.Type;
import db.BicycleDAO;
import db.TypeDAO;
import org.junit.*;

import java.time.LocalDate;

public class BicycleDAOTest {
    private BicycleDAO bicycleDAO = new BicycleDAO();
    private int typeID = 0;

    @Before
    public void setUp(){
        TypeDAO typeDAO = new TypeDAO();
        Type type = new Type("BicycleTest", 100);
        typeID = typeDAO.addType(type);
    }

    @Test
    public void testRegister()
    {
        Bicycle bike = new Bicycle();
        bike.setMake("TestMake");
        LocalDate date = LocalDate.now();
        bike.setDateRegistered(date);

        int id = bicycleDAO.addBicycle(bike, typeID);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);


        Bicycle newbike = bicycleDAO.getBicycle(id);

        Assert.assertEquals(id, newbike.getBicycleID());
        Assert.assertEquals("TestMake", newbike.getMake());
        Assert.assertEquals(date, newbike.getDate());
    }

    @Test
    public void testDelete()
    {
        Bicycle bike = new Bicycle();
        bike.setMake("TestMake");
        LocalDate date = LocalDate.now();
        bike.setDateRegistered(date);

        int id = bicycleDAO.addBicycle(bike, typeID);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);

        Bicycle newbike = bicycleDAO.getBicycle(id);
        Assert.assertNotNull(newbike);

        int ret = bicycleDAO.deleteBicycle(id);
        Assert.assertNotNull(ret);
        Assert.assertTrue(ret>0);
        newbike = bicycleDAO.getBicycle(id);
        Assert.assertNull(newbike);
    }

    @Test
    public void testSetStatuses()
    {
        Bicycle bike = new Bicycle();
        bike.setMake("TestMake");
        LocalDate date = LocalDate.now();
        bike.setDateRegistered(date);
        date = date.plusDays(1);

        int id = bicycleDAO.addBicycle(bike, typeID);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);

        bicycleDAO.setMakeorStatus(id, "Rented", 1);
        bicycleDAO.setMakeorStatus(id, "NewMake", 2);
        bicycleDAO.setTripsorDistorType(id, 10, 1);
        bicycleDAO.setBicycleDate(date,id);

        TypeDAO typeDAO = new TypeDAO();
        Type type = typeDAO.getType(typeID);

        Bicycle newbike = bicycleDAO.getBicycle(id);
        Assert.assertNotNull(newbike);
        System.out.println(newbike.getStatus());

        Assert.assertEquals("Rented",newbike.getStatus());
        Assert.assertEquals("NewMake",newbike.getMake());
        Assert.assertEquals(10,newbike.getTripCount());
        Assert.assertEquals(date,newbike.getDate());
    }
}
