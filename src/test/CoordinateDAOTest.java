package test;

import data.Bicycle;
import data.Coordinate;
import data.Type;
import db.BicycleDAO;
import db.CoordinateDAO;
import db.TypeDAO;
import org.junit.*;

import java.time.LocalDate;
import java.time.LocalTime;

public class CoordinateDAOTest{
        int bikeID = 0;

        @Before
        public void setUp(){
            TypeDAO typeDAO = new TypeDAO();
            Type type = new Type("RepairTest", 100);
            int typeID = typeDAO.addType(type);
            BicycleDAO bicycleDAO = new BicycleDAO();
            Bicycle bike = new Bicycle("RepairTest");
            bikeID = bicycleDAO.addBicycle(bike, typeID);
        }

        @Test
        public void testRegister()
        {
            CoordinateDAO coordinateDAO = new CoordinateDAO();
            LocalDate date = LocalDate.now();
            LocalTime time = LocalTime.now();

            Double lat = 10.100;
            Double lon = 20.200;

            Coordinate coord = new Coordinate(lat,lon,date,time);
            coordinateDAO.addCoordinates(coord, bikeID);

            Coordinate newCoordinate = coordinateDAO.getCoordinates(bikeID);
            Assert.assertNotNull(newCoordinate);

            Assert.assertEquals(10.100,newCoordinate.getLatitude(),0.1);
            Assert.assertEquals(20.200,newCoordinate.getLongitude(),0.1);
            Assert.assertEquals(date,newCoordinate.getDate());
            Assert.assertEquals(time.withNano(0),newCoordinate.getTime());
            return;

        }
}
