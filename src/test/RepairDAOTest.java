package test;

import data.Bicycle;
import data.Reparation;
import data.Type;
import db.BicycleDAO;
import db.RepairDAO;
import db.TypeDAO;
import org.junit.*;

import java.time.LocalDate;
import java.util.ArrayList;

public class RepairDAOTest {
    Reparation repair = new Reparation();
    RepairDAO repairDAO = new RepairDAO();
    private int bikeID = 0;
    private int typeID = 0;

    @Before
    public void setUp(){
        TypeDAO typeDAO = new TypeDAO();
        Type type = new Type("RepairTest", 100);
        typeID = typeDAO.addType(type);
        BicycleDAO bicycleDAO = new BicycleDAO();
        Bicycle bike = new Bicycle("RepairTest");
        bikeID = bicycleDAO.addBicycle(bike, typeID);
    }

    @Test
    public void testRepairs()
    {
        Reparation repair = new Reparation();
        LocalDate date = LocalDate.now();
        repair.setRepairReqDesc("Test");
        repair.setDateSent(date);

        int id = repairDAO.registerRepairs(repair, bikeID);
        Assert.assertNotNull(id);
        Assert.assertTrue(0<id);

        ArrayList<Reparation> newRepairs = repairDAO.getAllRepairs(bikeID);
        Reparation newRepair = newRepairs.get(0);

        Assert.assertEquals(id, newRepair.getRepairID());
        Assert.assertEquals("Test", newRepair.getRepairReqDesc());
        Assert.assertEquals(date, newRepair.getDateSent());

        repairDAO.returnBike(id,date,3000,"TestDone");

        ArrayList<Reparation> newRepairsDone = repairDAO.getAllRepairs(bikeID);
        Reparation newRepairDone = newRepairsDone.get(0);

        Assert.assertEquals(date, newRepairDone.getDateReturned());
        Assert.assertEquals(3000, newRepairDone.getPrice(),1);
        Assert.assertEquals("TestDone", newRepairDone.getRepairdesc());
        return;
    }



}
