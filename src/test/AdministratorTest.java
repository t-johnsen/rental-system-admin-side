package test;

import db.AdministratorDAO;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import util.LogIn;
import util.PasswordManager;

import static db.AdministratorDAO.*;

public class AdministratorTest{
	AdministratorDAO dao;
	
	@Before
	public void setup(){
		dao = new AdministratorDAO();
	}
	
	@Test
	public void testValidateEmail(){
		String testemail = "thisIsAnEmail@something.com";
		Assert.assertTrue(validEmailFormat(testemail));
		
	}
	
	@Test
	public void testValidateEmailFalse(){
		String testemail2 = "nota@Valid@email.com";
		Assert.assertFalse(validEmailFormat(testemail2));
	}
	
	@Test
	public void testSendingNewPassword(){
		String email = "monaullah98@gmail.com";
		String testPwd = PasswordManager.generateTempPassword();
		
		Assert.assertTrue(sendEmail(email, testPwd));
		
	}
	
	@Test
	public void testAddingNewAdmin(){
		String mail = "bicycle.rental.trondheim@gmail.com";
		
		Assert.assertTrue(addAdministrator(mail));
	}
	
	@Test
	public void testLoginAdmin(){
		String mail = "bicycle.rental.trondheim@gmail.com";
		String pWord = "0ff86460";
		Assert.assertTrue(LogIn.logIn(mail, pWord));
	}
	
}

