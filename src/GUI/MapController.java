package GUI;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;

import java.net.URL;
import java.text.DecimalFormat;
import java.text.Format;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;

//import com.lynden.gmapsfx.service.directions.DirectionsServiceCallback;
import com.lynden.gmapsfx.service.directions.*;
import data.Bicycle;
import data.Coordinate;
import javafx.beans.property.*;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;

public class MapController implements Initializable, MapComponentInitializedListener, DirectionsServiceCallback {

    private DecimalFormat formatter = new DecimalFormat("###.00000");

    public Label longitude;
    public Label latitude;
    private DirectionsService directionsService;
    DirectionsPane directionsPane;

    private LatLong latLong = null;

    @FXML
    private GoogleMapView mapView;
    private GoogleMap map;
    private Map<Integer, Marker> Bikelist;

    Marker newStation;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        mapView.addMapInializedListener(this);
        Bikelist = new HashMap<>();
    }

    @Override
    public void mapInitialized() {
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(63.429067, 10.392625))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(true)
                .zoomControl(true)
                .zoom(13);
                //.minZoom(11);
        map = mapView.createMap(mapOptions);

        /*directionsService = new DirectionsService();
        directionsPane = mapView.getDirec();
        DirectionsRequest directionsRequest = new DirectionsRequest(new LatLong(63.42858, 10.38776).toString(), new LatLong(63.43313, 10.40368).toString(), TravelModes.BICYCLING);
        directionsRequest.setOpt(false);
        directionsService.getRoute(directionsRequest, this, new DirectionsRenderer(true, mapView.getMap(), directionsPane));*/

        MarkerOptions markerOptions1 = new MarkerOptions();
        markerOptions1.position(new LatLong(63.4,10.35)).visible(true);
        MarkerOptions markerOptions2 = new MarkerOptions();
        markerOptions2.position(new LatLong(63.46,10.35)).visible(true);
        MarkerOptions markerOptions3 = new MarkerOptions();
        markerOptions3.position(new LatLong(63.4,10.5)).visible(true);
        MarkerOptions markerOptions4 = new MarkerOptions();
        markerOptions4.position(new LatLong(63.46,10.5)).visible(true);
        Marker corner1 = new Marker(markerOptions1);
        Marker corner2 = new Marker(markerOptions2);
        Marker corner3 = new Marker(markerOptions3);
        Marker corner4 = new Marker(markerOptions4);
        map.addMarker(corner1);
        map.addMarker(corner2);
        map.addMarker(corner3);
        map.addMarker(corner4);



        //.icon(getClass().getResource("../Images/bicycle.png").getPath());

        /*map.addMouseEventHandler(UIEventType.click, (GMapMouseEvent event) -> {
            latLong = event.getLatLong();
            if (newStation != null) map.removeMarker(newStation);
            if(latLong.getLongitude() > 10.5 || latLong.getLongitude() < 10.3 || latLong.getLatitude() < 63.4 || latLong.getLatitude() > 63.5){
                AlertHelper.showAlert(Alert.AlertType.ERROR, longitude.getScene().getWindow(), "Error", "Selected coordinates cannot be outside of Trondheim");
                return;
            }
            newStation = new Marker(new MarkerOptions().position(latLong));
            newStation.setAnimation(Animation.BOUNCE);
            map.addMarker(newStation);
            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content("<h5>Chosen coordinates<h5>"
                    +   "Current location: Lat - " + formatter.format(latLong.getLatitude()) + "  Long - " + formatter.format(latLong.getLongitude()) + "<br>");
            InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
            map.addUIEventHandler(newStation, UIEventType.click, (e) ->{
                bikeWindow.open(map,newStation);
            });
        });*/
    }

    public void addBikesInMap(){
        for (int i = 0; i<100; i++){
            Bicycle b = new Bicycle(i, 0, new Coordinate((Double)Math.random()*70,(Double)Math.random()*10), null, 0, null, null, 0, 0, null);
            addBikeMarker(b);
        }
    }

    public void rmBikesInMap(){
        for (int i = 0; i< 100; i++){
            map.removeMarker(Bikelist.get(i));
        }
    }

    public void changePositions(){
        for (int i = 0; i<100; i++){
            Bicycle b = new Bicycle(i, 0, new Coordinate((Double)Math.random()*70,(Double)Math.random()*10), null, 0, null, null, 0, 0, null);
            editBikeMarker(b);
        }
    }

    public void printCoordinates(){
        if(latLong != null){
            latitude.setText(formatter.format(latLong.getLatitude()));
            longitude.setText(formatter.format(latLong.getLongitude()));
        } else {
            AlertHelper.showAlert(Alert.AlertType.ERROR, longitude.getScene().getWindow(), "Error", "No coordinates selected");
            return;
        }
    }

    /*public void rmBikeMarker(Bicycle bike){
        MarkerOptions rmBikeOptions = new MarkerOptions().position(new LatLong(bike.getCoordinates().getLatitude(), bike.getCoordinates().getLongitude()));
        Marker rmBike = new Marker(rmBikeOptions);
        map.removeMarker(rmBike);
    }*/

    public void addBikeMarker(Bicycle bike){
        MarkerOptions newBikeOptions = new MarkerOptions().position(new LatLong(bike.getCoordinates().getLatitude(), bike.getCoordinates().getLongitude()));
        Marker newBike = new Marker(newBikeOptions);
        map.addMarker(newBike);
        Bikelist.put(bike.getBicycleID(), newBike);

        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h5> Bicycle ID: "+ bike.getBicycleID() + "<h5>"
                +   "Current location:" + bike.getCoordinates().toString() + "<br>");
        InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
        map.addUIEventHandler(newBike, UIEventType.click, (e) ->{
            bikeWindow.open(map,newBike);
        });
    }

    public void editBikeMarker(Bicycle bike){
        Marker editBike = Bikelist.get(bike.getBicycleID());
        editBike.setPosition(new LatLong(bike.getCoordinates().getLatitude(), bike.getCoordinates().getLongitude()));

    }

    @Override
    public void directionsReceived(DirectionsResult directionsResult, DirectionStatus directionStatus) {
        DirectionsRequest directionsRequest = new DirectionsRequest(new LatLong(63.42858, 10.38776).toString(), new LatLong(63.43313, 10.40368).toString(), TravelModes.BICYCLING);
        directionsService.getRoute(directionsRequest, this, new DirectionsRenderer(true, mapView.getMap(), directionsPane));
    }
}
