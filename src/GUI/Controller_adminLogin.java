package GUI;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.Window;
import util.LogIn;

import java.io.IOException;

import static db.AdministratorDAO.adminExists;
import static util.LogIn.forgottenPassword;


public class Controller_adminLogin {

    @FXML
    public GridPane login;
    public PasswordField password;
    public TextField username;
    public Button signInButton;
    public TextField rUsername;
    public Button resetPasswordButton;
    public GridPane resetPassword;
    @FXML
    private Button btnCancelResetP;

    @FXML
    public void signIn(ActionEvent e) throws IOException {
        Window owner = signInButton.getScene().getWindow();
        String uName = username.getText();
        String pWord = password.getText();
        if(uName.isEmpty() || uName.trim().isEmpty()){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Please enter a username");
            username.clear();
        }else if(pWord.isEmpty() || pWord.trim().isEmpty()){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Password field cannot be empty");
            password.clear();
        }else if (!LogIn.logIn(uName, pWord)){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Login failed. Wrong password og username");
            password.clear();
            username.clear();
        }else {
            Parent root;
            Stage stage = (Stage) signInButton.getScene().getWindow();
            root = FXMLLoader.load(getClass().getResource("mainPage.fxml"));
            Scene scene = new Scene(root, 1280, 720);
            stage.setScene(scene);
            stage.show();
        }

    }

    @FXML
    protected void back(){
        try{
            resetPassword.setVisible(false);
            login.setVisible(true);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    protected void resetTextPassword(){
        String username = rUsername.getText();
        Thread t = new Thread(()->{
            System.out.println(rUsername.getText());
            Window owner = resetPasswordButton.getScene().getWindow();
            if(!adminExists(username)){
                Platform.runLater(() -> {
                    AlertHelper.showWaitAlert(Alert.AlertType.INFORMATION, owner, "Failure", "Administrator email does not exist!");
                });
            } else if(forgottenPassword(username)){
                Platform.runLater(() -> {
                    AlertHelper.showWaitAlert(Alert.AlertType.INFORMATION, owner, "Success", "Password successfully reset");
                    back();
                });
            }
        
        });
        t.start();
    }

    @FXML
    public void resetPassword(){
        login.setVisible(false);
        resetPassword.setVisible(true);
    }
}
