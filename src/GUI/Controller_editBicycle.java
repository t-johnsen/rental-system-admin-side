package GUI;

/**
 * Controller for editing bicycle using GUI
 */

import data.Bicycle;
import data.Type;
import db.BicycleDAO;
import db.TypeDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.ResourceBundle;

public class Controller_editBicycle implements Initializable{

    public Button btnCancel;
    public ComboBox<Type> editTypeComboBox;
    public Label editPriceLabel;
    public Label editBicycleIdLabel;
    public TextField editModelTextField;
    public DatePicker editBicycleDatePicker;
    private Bicycle bike = new Bicycle();
    private Controller controller = new Controller();


    private Controller.Communciator communciator;

    public Controller_editBicycle(){
    }

    public void setCommunicator(Controller.Communciator communicator){
        this.communciator = communicator;
    }

    /**
     * Sets the global bike object, the bike that is to be edited
     * @param bike bicycle that is to be edited
     */
    public void setBike(Bicycle bike){
        this.bike = bike;
        editBicycleIdLabel.setText(String.valueOf(bike.getBicycleID()));
        editPriceLabel.setText(String.valueOf(bike.getPrice()));
        LocalDate now1 = bike.getDate();
        editBicycleDatePicker.setValue(now1);
        editModelTextField.setText(bike.getMake());
        editTypeComboBox.setValue(bike.getTypeObj());
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //System.out.println(bike.getBicycleID());
        TypeDAO typeDAO = new TypeDAO();
        ArrayList<Type> types = typeDAO.getAllType();
        ObservableList<Type> typeList = FXCollections.observableArrayList(types);
        editTypeComboBox.getItems().addAll(typeList);
        editTypeComboBox.valueProperty().addListener(new ChangeListener<Type>() {
            @Override
            public void changed(ObservableValue<? extends Type> observable, Type oldValue, Type newValue) {
                editPriceLabel.setText(String.valueOf(newValue.getPrice()));
            }
        });
    }


    @FXML
    public void cancel() {
        try {
            Stage stage = (Stage) btnCancel.getScene().getWindow();
            stage.close();
            //controller.bikeList();
        }catch(Exception e){
            e.printStackTrace();
        }
    }


    /**
     * Gets data from fields on page and edits the bicycle set on the method above
     */
    @FXML
    public void editBicycle() {
        LocalDate date = editBicycleDatePicker.getValue();
        String text = editModelTextField.getText();
        Type newType = editTypeComboBox.getValue();

        BicycleDAO bicycleDAO = new BicycleDAO();

        if(date != null && date.equals(bike.getDate())){
            if(bicycleDAO.setBicycleDate(date,bike.getBicycleID()) > 0){
                bike.setDateRegistered(date);
            }

        }
        if(text != null && !text.toLowerCase().equalsIgnoreCase(bike.getMake().toLowerCase())){
            if(bicycleDAO.setMakeorStatus(bike.getBicycleID(),text,2) > 0){
                bike.setMake(text);
            }

        }
        if(newType != null && !newType.equals(bike.getTypeObj())){
            if(bicycleDAO.setTripsorDistorType(bike.getBicycleID(),newType.getTypeID(),1) > 0){
                bike.setType(newType.getType());
            }
        }
        cancel();
    }






}