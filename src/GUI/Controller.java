package GUI;
/**
 * Controller for main page
 */

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import data.Bicycle;
import data.DockingStation;
import data.Reparation;
import data.Type;
import db.*;
import javafx.application.Platform;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.collections.transformation.FilteredList;
import javafx.collections.transformation.SortedList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.*;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;
import util.LogIn;
import util.PasswordManager;

import java.io.IOException;
import java.net.URL;
import java.text.DecimalFormat;
import java.time.LocalDate;
import java.util.*;

import static GUI.Controller_dockingStation.getcoords;
import static db.AdministratorDAO.getAdminId;
import static db.AdministratorDAO.validEmailFormat;
import static util.LogIn.getCurrAdmin;

/**
 * This Controller class is the main controller opening the main menu when administrator
 * logs into the system. From here much of the functionality of the GUI is connected to
 * the backend programming as well as the smaller controllers:
 * @see Controller_addAdmin
 * @see Controller_addBicycle
 * @see Controller_adminLogin
 * @see Controller_dockingStation
 * @see Controller_dockingStationMap
 * @see Controller_editBicycle
 * @see Controller_repairs
 * This main Controller is luanched after administrator has logged in.
 */

public class Controller implements Initializable, MapComponentInitializedListener{
//import javax.xml.soap.Text;

    private Window owner;
    private Stage stage;
    public AnchorPane box_home;
    public AnchorPane box_bicycles;
    public AnchorPane box_dockingStations;
    public AnchorPane box_settings;
    @FXML public TextField newemailTextfield;
    @FXML public TextField oldmailTextfield;
    @FXML public TextField newmailTextfieldRepeat;
    @FXML public Button changeButton;
    @FXML public PasswordField oldPasswordTextField;
    @FXML public PasswordField newPasswordTextField;
    @FXML public PasswordField newPasswordTextFieldRepeat;
    @FXML public Button changePasswordButton;
    public AnchorPane box_map;
    public AnchorPane box_bicycleSettings;
    public AnchorPane box_statistics;
    public AnchorPane box_repairs;

    @FXML
    private GoogleMapView mapView;
    private GoogleMap map;
    private Bicycle bike = new Bicycle();
    private boolean workshop = false;
    public TableView<Bicycle> bikeList;
    public Label BikeIDLabel;
    public Label BikeType;
    public Label bikePriceLabel;
    public Label bikeDateRegistered;
    public Label bikeBatteryPercent;
    public Label bikeStatusLabel;
    public Label bikeModelLabel;
    public Button editBikeLabel;
    public Button deleteBike;
    public Button markAsDamaged;
    public Button searchListButton;
    public TextField bikeSearchText;
    public Button addBicycle;


    public TableView<DockingStation> dockingList;
    public Label dockID;
    public Label dockNames;
    public Label dockCapacity;
    public TextField dockSearch;
    public Button addDockingstation;
    public Button editDockingStation;
    public Button btnDeleteDocking;
    public TableView<Bicycle> slotList;

    public TableView<Bicycle> repairBikeList;
    public TextField bikeRepairSearch;
    public CheckBox damagedCheckBox;
    public TableView<Reparation> repairsListBike;
    public Label RepairBikeIDLabel;
    public Label RepairBikeType;
    public Label RepairsBikeModelLabel;
    public Label RepairBikePriceLabel;
    public Label RepairBikeDateRegistered;
    public Label RepairBikeBatteryPercent;
    public Label RepairBikeStatusLabel;

    public Label RepairIDLabel;
    public Label RepairDateSentLabel;
    public Label RepairsDateReturnedLabel;
    public Label RepairReqDesLabel;
    public Label RepairPriceLabel;
    public Label RepairDesLabel;
    public Button btnRequestRepair;
    public Button btnRegisterFinishedRepairs;

    public TableView typeTable;
    public TextField bicycleSettingsSearchTextField;
    public Button btnAddType;
    public TextField addTypeTextField;
    public TextField addPriceTextField;

    public CategoryAxis yAxis ;
    public BarChart<String, Integer> economyChart;

    public LineChart<String, Integer> bikeRentedChart;
    public CategoryAxis BikeyAxis;
    public NumberAxis BikexAxis;
    public Label rentalsTitle;
    public Label economyTitle;
    public BarChart<String, Integer> generalChart;
    public CategoryAxis generalyAxis;
    public NumberAxis generalxAxis;
    public Label generalTitle;

    SlotDAO slotDAO = new SlotDAO();
    RentalDAO RentalDAO = new RentalDAO();
    Timer t = new Timer();

    private Marker newStation;
    private LatLong latLong;
    private DecimalFormat formatter = new DecimalFormat("###.00000");


    private Map<Integer, Marker> bikemap;
    private Map<Integer, Marker> dockmap;
    static boolean newDocking = false;
    static boolean updatedocking = false;
    static private double editDockingLong;
    static private double editDockingLat;
    static private int editDockCapacity;
    static private String editDockName;
    static private int editDockID;
    private Marker workshopMarker = null;

    public Button btnEditTypePrice;
    public Button btnDeleteType;
    public TextField newTypeTextField;

    public Button btnHome;
    public Button btnBicycles;
    public Button btnDockingStations;
    public Button btnMap;
    public Button btnBicycleSettings;
    public Button btnStatistics;
    public Button btnSettings;
    public Button btnRepairs;



    BicycleDAO bicycleDAO = new BicycleDAO();
    ArrayList<Bicycle> bikes = new ArrayList<>();
    ObservableList<Bicycle> list = FXCollections.observableArrayList(bikes);

    ArrayList<Bicycle> RepairBikes = bikes;
    ObservableList<Bicycle> repairList = FXCollections.observableArrayList(RepairBikes);

    ArrayList<Reparation> repairs = new ArrayList<>();
    ObservableList<Reparation> bikeRepairsList = FXCollections.observableArrayList(repairs);

    ArrayList<Bicycle> bikesInDock = new ArrayList<>();
    ObservableList<Bicycle> bikesInDockList = FXCollections.observableArrayList(bikesInDock);

    DockingStationDAO dockDao = new DockingStationDAO();
    ArrayList<DockingStation> dockings = new ArrayList<>();
    ObservableList<DockingStation> dList = FXCollections.observableArrayList(dockings);

    TypeDAO typeDAO= new TypeDAO();
    ArrayList<Type> typeArrayList = new ArrayList<>();
    ObservableList<Type> typeList = FXCollections.observableArrayList(typeArrayList);


    /**
     * Initializes the mainPage fxml-file
     * @param location dunno
     * @param resources dunno
     */
    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("INITIALIZING");
        System.out.println("View is now loaded!");
        changePasswordButton.setDisable(true);
        mapView.addMapInializedListener(this);
        mapView.addMapReadyListener(()->{
            bikemap = new HashMap<>();
            dockmap = new HashMap<>();
            dockList();
            bikeList();



            map.addMouseEventHandler(UIEventType.click, (GMapMouseEvent event) -> {
                if(stage != null){
                latLong = event.getLatLong();
                if (newStation != null) map.removeMarker(newStation);
                if (latLong.getLongitude() > 10.5 || latLong.getLongitude() < 10.3 || latLong.getLatitude() < 63.4 || latLong.getLatitude() > 63.5) {
                    AlertHelper.showAlert(Alert.AlertType.ERROR, mapView.getScene().getWindow(), "Error", "Selected coordinates cannot be outside of Trondheim");
                    return;
                }
                newStation = new Marker(new MarkerOptions()
                        .position(latLong)
                        .animation(Animation.BOUNCE));
                map.addMarker(newStation);
                InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                infoWindowOptions.content("<h5>Chosen coordinates<h5>"
                        + "Current location: Lat - " + formatter.format(latLong.getLatitude()) + "  Long - " + formatter.format(latLong.getLongitude()) + "<br>");
                InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
                map.addUIEventHandler(newStation, UIEventType.click, (e) -> {
                    bikeWindow.open(map, newStation);
                    System.out.println("1");});
                Platform.runLater(()->{
                        chooseCoordinates(stage);
                        map.removeMarker(newStation);
                });
                }
            });
        });
        populateEconomyStatistics();
        bikemap = new HashMap<>();
        dockmap = new HashMap<>();
        typeList();
    
        t.scheduleAtFixedRate(
                new TimerTask()
                {
                    public void run()
                    {
                        Platform.runLater(()-> {
                            updateElements();
                            System.out.println("From thread************************************************");
                        });
                    }
                },
                40*1000,      // run first occurrence after forty seconds
                1000*40);  // run every forty seconds*/
        
    }

    /**
     * Method for getting all dockingstations from database and adding them to the tableView for dockingstations
     * Also contains the code for search function on docking station page
     */
    private void updateDockList(){
        Thread dockThread = new Thread(()->{
            dockings = dockDao.getAllDockingStations();
            dList = FXCollections.observableArrayList(dockings);
            Platform.runLater(() -> {
                dockingList.setItems(dList);
                addDockings();

                //************ SEARCH DOCKINGS ***********//
                FilteredList<DockingStation> filteredData = new FilteredList<>(dList, p -> true);
                dockSearch.textProperty().addListener((observable, oldValue, newValue) -> {
                    filteredData.setPredicate(myObject -> {
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        String filter = newValue.toLowerCase();

                        if (String.valueOf(myObject.getDockingID()).contains(filter)) {
                            return true;
                        }else if ((myObject.getName()).toLowerCase().contains(filter)) {
                            return true;
                        }

                        return false;
                    });
                });
                SortedList<DockingStation> sortedData = new SortedList<>(filteredData);
                sortedData.comparatorProperty().bind(dockingList.comparatorProperty());
                dockingList.setItems(sortedData);
            });
        });
        dockThread.start();
    }


    @FXML
    private void generalStatistics(){
        bikeRentedChart.setVisible(false);
        rentalsTitle.setVisible(false);
        economyTitle.setVisible(false);
        economyChart.setVisible(false);
        generalChart.setVisible(true);
        generalTitle.setVisible(true);
        generalChart.getData().clear();

        LocalDate date = LocalDate.now();
        XYChart.Series statistics = new XYChart.Series<>();

        statistics.setName("Statistics");
        int trips = 0;
        int distance = 0;
        int kmd = 0;
        int power = 0;
        for(Bicycle b:bikes){
            trips += b.getTripCount();
            distance += b.getDistanceTravelled();
        }
        for(DockingStation d:dockings){
            power += d.getPowerUsage();
        }
        if(trips != 0){
            kmd = distance/trips;
        }
        String tripsString = "Trips";
        String distanceString = "Distance";
        String kmdString = "KM/D";

        statistics.getData().add(new XYChart.Data(tripsString,trips));
        //statistics.getData().add(new XYChart.Data(distanceString,distance));
        statistics.getData().add(new XYChart.Data(kmdString,kmd));

        generalChart.getData().addAll(statistics);
        generalChart.setBarGap(3);
        generalChart.setCategoryGap(20);
    }

    @FXML
    private void rentedStatistics(){
        bikeRentedChart.setVisible(true);
        economyChart.setVisible(false);
        rentalsTitle.setVisible(true);
        economyTitle.setVisible(false);
        generalChart.setVisible(false);
        generalTitle.setVisible(false);
        bikeRentedChart.getData().clear();

        int nbBikesRented = 0;
        LocalDate date = LocalDate.now();
        XYChart.Series bikes = new XYChart.Series<>();
        bikes.setName("Bikes Rented");
        for(int i = 0; i < 7; i++){
            nbBikesRented = RentalDAO.getRental(date);
            date = date.plusDays(1);
            bikes.getData().add(new XYChart.Data(date.toString(),nbBikesRented));
        }
        bikeRentedChart.getData().add(bikes);
    }

    @FXML
    private void populateEconomyStatistics(){
        bikeRentedChart.setVisible(false);
        rentalsTitle.setVisible(false);
        economyTitle.setVisible(true);
        economyChart.setVisible(true);
        generalChart.setVisible(false);
        generalTitle.setVisible(false);
        economyChart.getData().removeAll();
        economyChart.getData().clear();

        //int[] bikesRented = new int[7];
        LocalDate date = LocalDate.now();
        XYChart.Series incomeS = new XYChart.Series<>();
        XYChart.Series expensesS = new XYChart.Series<>();
        XYChart.Series profitS = new XYChart.Series<>();
        incomeS.setName("Income");
        expensesS.setName("Expenses");
        profitS.setName("Profit");
        int income = 0;
        int expence = 0;
        int profit = 0;
        for(int i = 0; i < 7; i++){

            income = RentalDAO.getIncome(date);
            expence = RentalDAO.getExpenses(date);
            profit = income-expence;
            date = date.plusDays(1);
            incomeS.getData().add(new XYChart.Data(date.toString(),income));
            expensesS.getData().add(new XYChart.Data(date.toString(),expence));
            profitS.getData().add(new XYChart.Data(date.toString(),profit));

        }

        economyChart.getData().addAll(incomeS,expensesS,profitS);
        economyChart.setBarGap(3);
        economyChart.setCategoryGap(20);
    }

    /**
     * Metod for getting all bikes that are docked in a dockingstation
     * @param dock  Dockingsation object that you wish to get bicycles docked in
     */
    private void getBikesInDock(DockingStation dock){
        Thread bikesInDockThread = new Thread(() -> {
            bikesInDock = dockDao.getBicyclesInDockingStation(dock.getDockingID());
            bikesInDockList = FXCollections.observableArrayList(bikesInDock);

            Platform.runLater(()-> {
                slotList.setItems(bikesInDockList);
            });
        });
        bikesInDockThread.start();
    }

    /**
     * Creates sets up tableView for docking list.
     * Contains methods for getting selected items from list, adding, editing and deleting dockingstation
     */
    private void dockList(){
        editDockingStation.setDisable(true);
        btnDeleteDocking.setDisable(true);

        updateDockList();
        dockingList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        System.out.println("CREATE NEW");

        TableColumn<DockingStation, String> dockIDLabel = new TableColumn<>("ID");
        TableColumn<DockingStation, String> dockName = new TableColumn<>("Name");
        TableColumn<DockingStation, String> powerUsed = new TableColumn<>("Power usage");
        dockName.setMinWidth(20);
        dockName.setMinWidth(150);
        powerUsed.setMinWidth(70);

        dockIDLabel.setCellValueFactory(new PropertyValueFactory<>("dockingID"));
        dockName.setCellValueFactory(new PropertyValueFactory<>("name"));
        powerUsed.setCellValueFactory(new PropertyValueFactory<>("powerUsage"));

        dockingList.getColumns().addAll(dockIDLabel, dockName, powerUsed);

        TableColumn<Bicycle, String> dockBikeID = new TableColumn<>("ID:");
        TableColumn<Bicycle, String> dockBikeBattery = new TableColumn<>("Battery:");

        dockBikeID.setCellValueFactory(new PropertyValueFactory<>("bicycleID"));
        dockBikeBattery.setCellValueFactory(new PropertyValueFactory<>("chargingLvl"));

        slotList.getColumns().addAll(dockBikeID, dockBikeBattery);

        //****************** SLOTLIST ********************//
        dockingList.getSelectionModel().selectedItemProperty().addListener(i ->{
            DockingStation newValues = dockingList.getSelectionModel().getSelectedItem();
            if(newValues != null){
                editDockingStation.setDisable(false);
                btnDeleteDocking.setDisable(false);

                dockID.setText(String.valueOf(newValues.getDockingID()));
                dockNames.setText(newValues.getName());
                dockCapacity.setText(String.valueOf(newValues.getCapacity()));

                editDockingStation.setOnAction(new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        goTo_editDockingStation(newValues);
                    }
                });

                getBikesInDock(newValues);
            }
        });

        btnDeleteDocking.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
            @Override
            public void handle(javafx.event.ActionEvent event) {
                ObservableList<Integer> selectedIndexes = dockingList.getSelectionModel().getSelectedIndices();
                String dockIDs = "";
                for(int docks:selectedIndexes){
                    dockIDs += dList.get(docks).getDockingID() + ", ";
                }

                Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete dockingstations: " + dockIDs + " ?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                alert.showAndWait();

                if (alert.getResult() == ButtonType.YES) {
                    deleteDockingStations(dockingList.getSelectionModel().getSelectedItems());
                    dockingList.getSelectionModel().clearSelection();
                }

            }
        });


    }

    /**
     * Method for getting all bicycles from database and adding them to the bicycle table view and bicycle repair table view
     * Contain code for searching through both lists mentioned above
     */
    private void updateBikeList(){
        Thread bikeThread = new Thread(()-> {
            bikes = bicycleDAO.getAllBicycles();
            list = FXCollections.observableArrayList(bikes);
            RepairBikes = bikes;

            repairList = FXCollections.observableArrayList(RepairBikes);
            Platform.runLater(() -> {
                bikeList.setItems(list);
                repairBikeList.setItems(repairList);
                addBikes();
    
                // ******************* SEARCH BIKES ***************** //
                FilteredList<Bicycle> filteredData = new FilteredList<>(list, p -> true);

                bikeSearchText.textProperty().addListener((observable, oldValue, newValue) -> {
                    filteredData.setPredicate(myObject -> {
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        String filter = newValue;

                        if (String.valueOf(myObject.getBicycleID()).contains(filter)) {
                            return true;

                        }

                        return false;
                    });
                });

                SortedList<Bicycle> sortedData = new SortedList<>(filteredData);

                sortedData.comparatorProperty().bind(bikeList.comparatorProperty());
                bikeList.setItems(sortedData);

                // ******************* SEARCH DAMAGED ***************** //
                FilteredList<Bicycle> filteredDataR = new FilteredList<>(repairList, p -> true);

                bikeRepairSearch.textProperty().addListener((observable, oldValue, newValue) -> {
                    filteredDataR.setPredicate(myObject -> {
                        if (newValue == null || newValue.isEmpty()) {
                            return true;
                        }

                        String filterR = newValue.toLowerCase();

                        if (String.valueOf(myObject.getBicycleID()).contains(filterR)) {
                            return true;
                        }

                        if (myObject.getStatus().toLowerCase().contains(filterR)) {
                            return true;
                        }

                        return false;
                    });
                });

                damagedCheckBox.selectedProperty().addListener(new ChangeListener<Boolean>() {
                    @Override
                    public void changed(ObservableValue<? extends Boolean> observable, Boolean oldValue, Boolean newValue) {
                        filteredDataR.setPredicate(myObject -> {

                            if (newValue && myObject.getStatus().equalsIgnoreCase("damaged")) {
                                return true;
                            }

                            return !newValue;
                        });
                    }
                });

                SortedList<Bicycle> sortedDataR = new SortedList<>(filteredDataR);
                sortedDataR.comparatorProperty().bind(repairBikeList.comparatorProperty());
                repairBikeList.setItems(sortedDataR);
            });
        });
        bikeThread.start();
    }

    /**
     * Method for setting up tableView for tables in bicycles and repairs page.
     * Contains code for buttons that allow adding, deleting, editing bicycles as well as registering and finalising repairs
     */
    private void bikeList(){

        deleteBike.setDisable(true);
        changeButton.setDisable(true);
        editBikeLabel.setDisable(true);
        list = FXCollections.observableArrayList();

        updateBikeList();

        bikeList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        TableColumn<Bicycle, String> bikeID = new TableColumn<>("ID");
        TableColumn<Bicycle, String> bikeStatus = new TableColumn<>("Status");
        TableColumn<Bicycle, String> bikePower = new TableColumn<>("Battery");
        bikeID.setMinWidth(100);
        bikeStatus.setMinWidth(150);
        bikePower.setMinWidth(100);

        bikeID.setCellValueFactory(new PropertyValueFactory<>("bicycleID"));
        bikeStatus.setCellValueFactory(new PropertyValueFactory<>("status"));
        bikePower.setCellValueFactory(new PropertyValueFactory<>("chargingLvl"));

        bikeList.getColumns().addAll(bikeID, bikeStatus, bikePower);



            bikeList.getSelectionModel().selectedItemProperty()
                    .addListener(new ChangeListener<Bicycle>() {

                        @Override
                        public void changed(ObservableValue<? extends Bicycle> observable, Bicycle oldValue, Bicycle newValue) {
                            deleteBike.setDisable(false);
                            editBikeLabel.setDisable(false);
                            if (newValue != null) {
                                BikeIDLabel.setText(String.valueOf(newValue.getBicycleID()));
                                bikeModelLabel.setText(newValue.getMake());
                                BikeType.setText(newValue.getType());
                                bikePriceLabel.setText(String.valueOf(newValue.getPrice()));
                                bikeDateRegistered.setText(newValue.getDateString());
                                bikeStatusLabel.setText(newValue.getStatus());
                                bikeBatteryPercent.setText(String.valueOf(newValue.getChargingLvl()));

                                editBikeLabel.setOnAction(new EventHandler<javafx.event.ActionEvent>() {

                                    @Override
                                    public void handle(javafx.event.ActionEvent event) {
                                        goTo_editBicycle(newValue);
                                    }
                                });
                            }
                        }
                    });

            deleteBike.setOnAction(new EventHandler<javafx.event.ActionEvent>() {
                @Override
                public void handle(javafx.event.ActionEvent event) {
                    ObservableList<Integer> selectedIndexes = bikeList.getSelectionModel().getSelectedIndices();
                    String bikesIDs = "";
                    for (int bikes : selectedIndexes) {
                        bikesIDs += list.get(bikes).getBicycleID() + ", ";
                    }

                    Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete bicycles: " + bikesIDs + " ?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                    alert.showAndWait();

                    if (alert.getResult() == ButtonType.YES) {
                        deleteBicycle(selectedIndexes);
                        bikeList.getSelectionModel().clearSelection();
                    }

                }
            });

            addBicycle.setOnAction(new EventHandler<ActionEvent>() {
                @Override
                public void handle(ActionEvent event) {
                    if(slotDAO.countSlots() > list.size()){
                        goTo_addBicycle();
                    }else{
                        owner = addBicycle.getScene().getWindow();
                        AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "More bicycles than slots, to add a bicycle add more slots");
                    }
                }
            });

            //****************REPAIR PAGE***************//

        btnRequestRepair.setDisable(true);
        btnRegisterFinishedRepairs.setDisable(true);

            TableColumn<Reparation, String> repairID = new TableColumn<>("ID");
            TableColumn<Reparation, LocalDate> repairDateSent = new TableColumn<>("Date Sent");
            repairID.setMinWidth(50);
            repairDateSent.setMinWidth(90);


            repairID.setCellValueFactory(new PropertyValueFactory<>("repairID"));
            repairDateSent.setCellValueFactory(new PropertyValueFactory<>("dateSent"));

            repairsListBike.getColumns().addAll(repairID, repairDateSent);

            RepairBikes = bikes;

            repairList = FXCollections.observableArrayList(RepairBikes);

            repairBikeList.setItems(repairList);
            repairBikeList.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

            TableColumn<Bicycle, String> bikeIDR = new TableColumn<>("ID");
            TableColumn<Bicycle, String> bikeStatusR = new TableColumn<>("Status");
            TableColumn<Bicycle, String> bikePowerR = new TableColumn<>("Battery");
            bikeIDR.setMinWidth(100);
            bikeStatusR.setMinWidth(150);
            bikePowerR.setMinWidth(100);

            bikeIDR.setCellValueFactory(new PropertyValueFactory<>("bicycleID"));
            bikeStatusR.setCellValueFactory(new PropertyValueFactory<>("status"));
            bikePowerR.setCellValueFactory(new PropertyValueFactory<>("chargingLvl"));

            repairBikeList.getColumns().addAll(bikeIDR, bikeStatusR, bikePowerR);

            RepairDAO repairDAO = new RepairDAO();


            repairBikeList.getSelectionModel().selectedItemProperty()
                    .addListener(new ChangeListener<Bicycle>() {

                        @Override
                        public void changed(ObservableValue<? extends Bicycle> observable, Bicycle oldValue, Bicycle newValue) {
                            if (newValue != null) {
                                btnRequestRepair.setDisable(false);
                                btnRequestRepair.setOnAction(new EventHandler<ActionEvent>() {
                                    @Override
                                    public void handle(ActionEvent event) {
                                        goTo_registerRepReq(newValue);
                                    }
                                });
                                RepairBikeIDLabel.setText(String.valueOf(newValue.getBicycleID()));
                                RepairBikeType.setText(newValue.getType());
                                RepairsBikeModelLabel.setText(newValue.getMake());
                                RepairBikePriceLabel.setText(String.valueOf(newValue.getPrice()));
                                RepairBikeDateRegistered.setText(newValue.getDateString());
                                RepairBikeBatteryPercent.setText(String.valueOf(newValue.getChargingLvl()));
                                RepairBikeStatusLabel.setText(newValue.getStatus());
                                repairsListBike.getSelectionModel().clearSelection();
                                RepairIDLabel.setText("-");
                                RepairDateSentLabel.setText("-");
                                RepairsDateReturnedLabel.setText("-");
                                RepairReqDesLabel.setText("-");
                                RepairPriceLabel.setText("-");
                                RepairDesLabel.setText("-");

                                repairs = repairDAO.getAllRepairs(newValue.getBicycleID());
                                bikeRepairsList = FXCollections.observableArrayList(repairs);

                                repairsListBike.setItems(bikeRepairsList);
                                repairsListBike.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

                                repairsListBike.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Reparation>() {
                                    @Override
                                    public void changed(ObservableValue<? extends Reparation> observable, Reparation oldValue, Reparation newValue) {
                                        if (newValue != null) {
                                            btnRegisterFinishedRepairs.setDisable(false);
                                            RepairIDLabel.setText(String.valueOf(newValue.getRepairID()));
                                            RepairDateSentLabel.setText(newValue.getDateSent().toString());
                                            if (newValue.getDateReturned() != null) {
                                                RepairsDateReturnedLabel.setText(newValue.getDateReturned().toString());
                                            } else {
                                                RepairsDateReturnedLabel.setText("-");
                                            }
                                            RepairReqDesLabel.setText(newValue.getRepairReqDesc());
                                            if (newValue.getPrice() != 0) {
                                                RepairPriceLabel.setText(String.valueOf(newValue.getPrice()));
                                            } else {
                                                RepairPriceLabel.setText("-");
                                            }
                                            if (newValue.getRepairdesc() != null) {
                                                RepairDesLabel.setText(newValue.getRepairdesc());
                                            } else {
                                                RepairDesLabel.setText("-");
                                            }

                                            // FINALIZE REPAIRS
                                            btnRegisterFinishedRepairs.setOnAction(new EventHandler<ActionEvent>() {
                                                @Override
                                                public void handle(ActionEvent event) {
                                                    goTo_registerRep(newValue);
                                                }
                                            });


                                        }
                                    }
                                });
                            }
                        }
                    });





    }

    /**
     * Method for setting up tableView containg all lists.
     * Contains code for adding new type
     */
    private void typeList(){
        getTypeList();
        btnEditTypePrice.setDisable(true);
        btnDeleteType.setDisable(true);

        TableColumn<Type, String> typeName = new TableColumn<>("Type:");
        TableColumn<Type, Integer> typePrice = new TableColumn<>("Price:");
        TableColumn<Type, Integer> typeID = new TableColumn<>("ID:");

        typeID.setCellValueFactory(new PropertyValueFactory<>("typeID"));
        typeName.setCellValueFactory(new PropertyValueFactory<>("type"));
        typePrice.setCellValueFactory(new PropertyValueFactory<>("price"));
        typeTable.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);

        typeTable.getColumns().addAll(typeID, typeName,typePrice);

        typeTable.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Type>() {
            @Override
            public void changed(ObservableValue<? extends Type> observable, Type oldValue, Type newValue) {
                if(newValue != null){
                    btnDeleteType.setDisable(false);
                    btnEditTypePrice.setDisable(false);
                    Type newType = newValue;


                    btnEditTypePrice.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            editTypePrice(newType);
                        }
                    });

                    btnDeleteType.setOnAction(new EventHandler<ActionEvent>() {
                        @Override
                        public void handle(ActionEvent event) {
                            ObservableList<Integer> selectedIndexes = typeTable.getSelectionModel().getSelectedIndices();
                            String typeIDs = "";
                            for (int t : selectedIndexes) {
                                typeIDs += typeList.get(t).getTypeID() + ", ";
                            }

                            Alert alert = new Alert(Alert.AlertType.CONFIRMATION, "Delete types: " + typeIDs + " ?", ButtonType.YES, ButtonType.NO, ButtonType.CANCEL);
                            alert.showAndWait();

                            if (alert.getResult() == ButtonType.YES) {
                                deleteType(selectedIndexes);
                                typeTable.getSelectionModel().clearSelection();
                            }
                        }
                    });

                }
            }
        });

        btnAddType.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {
                int price = 0;
                owner = btnAddType.getScene().getWindow();
                if(addPriceTextField.getText() != null && !addPriceTextField.getText().trim().equalsIgnoreCase("")){
                    price = Integer.parseInt(addPriceTextField.getText());
                }else{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter a price");
                }
                String type = addTypeTextField.getText();
                if(type == null || type.trim().equalsIgnoreCase("")){
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter a type");
                    return;
                }

                Type newType = new Type(type, price);
                int ret = typeDAO.addType(newType);
                if(ret < 0){
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not add type");
                }else{
                    newType = typeDAO.getType(ret);
                    typeList.addAll(newType);
                    addTypeTextField.clear();
                    addPriceTextField.clear();
                }
            }
        });



    }


    /**
     * Method for getting all types from database and adding them to tableView for types
     * Contains code that makes search function on bicycle setting work
     */
    private void getTypeList(){
        Thread typeListThread = new Thread(() -> {
           typeArrayList = typeDAO.getAllType();
           typeList = FXCollections.observableArrayList(typeArrayList);

           Platform.runLater(() -> {
               typeTable.setItems(typeList);

               FilteredList<Type> filteredTypeData = new FilteredList<>(typeList, p -> true);

               bicycleSettingsSearchTextField.textProperty().addListener((observable, oldValue, newValue) -> {
                   filteredTypeData.setPredicate(myObject -> {
                       if (newValue == null || newValue.isEmpty()) {
                           return true;
                       }

                       String typeFilter = newValue.toLowerCase();

                       if (String.valueOf(myObject.getTypeID()).contains(typeFilter)) {
                           return true;
                       } else if ((myObject.getType().toLowerCase()).contains(typeFilter)){
                           return true;
                       }

                       return false;
                   });
               });

               SortedList<Type> sortedTypeData = new SortedList<>(filteredTypeData);

               sortedTypeData.comparatorProperty().bind(typeTable.comparatorProperty());
               typeTable.setItems(sortedTypeData);
            });
        });
        typeListThread.start();
    }

    /**
     * Standard method for initializing the map
     */
    @Override
    public void mapInitialized(){
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(63.429067, 10.392625))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(true)
                .zoom(13)
                .minZoom(11)
                .maxZoom(20)
                .mapTypeControl(false);
        try{
            map = mapView.createMap(mapOptions);
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Changes view to the map and zooms in on den selected docking station
     */
    public void showDockInMap(){
        boolean infobox = true;
        int selectedIndex = dockingList.getSelectionModel().getSelectedIndex();
        DockingStation dock = dList.get(selectedIndex);
        goTo_map();
        if (dockmap.get(dock.getDockingID()) == null){
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLong(dock.getCoordinates().getLatitude(), dock.getCoordinates().getLongitude()))
                    .icon("clustered/images/m1.png");
            Marker marker = new Marker(markerOptions);
            map.addMarker(marker);
            dockmap.put(dock.getDockingID(), marker);
            infobox = false;
        }
        mapView.setZoom(13);
        mapView.setCenter(dock.getCoordinates().getLatitude(), dock.getCoordinates().getLongitude());
        mapView.setZoom(15);
        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
        infoWindowOptions.content("<h5>Information<h5>"
                +   "Name: " + dock.getName()
                +   "<br>Docking station ID: " + dock.getDockingID()
                +   "<br>Location: Lat - " + formatter.format(dock.getCoordinates().getLatitude()) + "  Long - " + formatter.format(dock.getCoordinates().getLongitude())
                +   "<br>Power usage: " + dock.getPowerUsage() + "<br>");
        InfoWindow dockWindow = new InfoWindow(infoWindowOptions);
        dockWindow.open(map, dockmap.get(dock.getDockingID()));
        if (!infobox){
            map.addUIEventHandler(dockmap.get(dock.getDockingID()), UIEventType.click, (e) ->{
                dockWindow.open(map, dockmap.get(dock.getDockingID()));
            });
        }
    }

    /**
     * Changes view to the map and zooms in on den selected bike
     */
    public void showBikeInMap(){
        boolean infobox = true;
        int selectedIndex = bikeList.getSelectionModel().getSelectedIndex();
        Bicycle bike = list.get(selectedIndex);
        goTo_map();
        if(bikemap.get(bike.getBicycleID()) == null && bike.getStatus().toLowerCase().equals("rented") && bike.getCoordinates().getLatitude() != null && bike.getCoordinates().getLongitude() != null){
            MarkerOptions markerOptions = new MarkerOptions()
                    .position(new LatLong(bike.getCoordinates().getLatitude(), bike.getCoordinates().getLongitude()))
                    .icon("clustered/images/bikeMarker.png");
            Marker marker = new Marker(markerOptions);
            map.addMarker(marker);
            bikemap.put(bike.getBicycleID(), marker);
            infobox = false;
        }
        mapView.setZoom(13);
        if(bike.getCoordinates().getLongitude() != null && bike.getCoordinates().getLatitude() != null){
            mapView.setCenter(bike.getCoordinates().getLatitude(), bike.getCoordinates().getLongitude());
            mapView.setZoom(15);
            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content("<h5>Information<h5>"
                    +   "Bicycle ID: " + bike.getBicycleID()
                    +   "<br>Status: " + bike.getStatus()
                    +   "<br>Current location: Lat - " + formatter.format(bike.getCoordinates().getLatitude()) + "  Long - " + formatter.format(bike.getCoordinates().getLongitude()) + "<br>"
                    +   "<br>Tripcount: " + bike.getTripCount() + "    Distance travelled: " + bike.getDistanceTravelled());
            InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
            bikeWindow.open(map,bikemap.get(bike.getBicycleID()));
            if(!infobox){
                map.addUIEventHandler(bikemap.get(bike.getBicycleID()), UIEventType.click, (e) ->{
                    bikeWindow.open(map,bikemap.get(bike.getBicycleID()));
                });
            }
        }
    }

    /**
     * Switches mapcenter between the workshop in Oslo and Trondheim
     */
    public void goToWorkshop(){
        if (!workshop) {
            map.setCenter(new LatLong(59.946028, 10.765223));
            workshop = true;
        }
        else {
            map.setCenter(new LatLong(63.429067, 10.392625));
            workshop = false;
        }
    }

    /**
     * Adds all docking stations as custom markers in the map, is run when initialized
     */
    public void addDockings(){
        final int[] bikesAtWorkshop = {0};
        if(workshopMarker == null){
            MarkerOptions workshopOptions = new MarkerOptions().position(new LatLong(59.946028, 10.765223)).icon("clustered/images/m2.png");
            workshopMarker = new Marker(workshopOptions);
            map.addMarker(workshopMarker);
            map.addUIEventHandler(workshopMarker, UIEventType.click, (e) -> {
                bikesAtWorkshop[0] = 0;
                for (int i = 0; i< bikes.size(); i++){
                    if (bikes.get(i).getStatus().toLowerCase().equals("repair")) bikesAtWorkshop[0]++;
                }
                InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                infoWindowOptions.content("<b><h5>Oslo sykkelverksted<h5><b>"
                        + "<br>Bikes at workshop: " + bikesAtWorkshop[0]
                        + "<br>Location: 59.946028, 10.765223");
                InfoWindow workshopWindow = new InfoWindow(infoWindowOptions);
                workshopWindow.open(map, workshopMarker);
            });
        }

        for (DockingStation d : dockings){
            if(dockmap.get(d.getDockingID()) == null){
                MarkerOptions markerOptions = new MarkerOptions()
                        .position(new LatLong(d.getCoordinates().getLatitude(), d.getCoordinates().getLongitude()))
                        .icon("clustered/images/m1.png");
                Marker marker = new Marker(markerOptions);
                map.addMarker(marker);
                dockmap.put(d.getDockingID(), marker);

                map.addUIEventHandler(marker, UIEventType.click, (e) ->{
                    InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                    infoWindowOptions.content("<h5>Information<h5>"
                            +   "Name: " + d.getName()
                            +   "<br>Docking station ID: " + d.getDockingID()
                            +   "<br>Location: Lat - " + formatter.format(d.getCoordinates().getLatitude()) + "  Long - " + formatter.format(d.getCoordinates().getLongitude())
                            +   "<br>Power usage: " + d.getPowerUsage() + "<br>");
                    InfoWindow dockWindow = new InfoWindow(infoWindowOptions);
                    dockWindow.open(map,marker);
                });
            }
        }
    }

    /**
     * Adds all bikes as custom markers in the map, is run when initialized
     */
    public void addBikes(){
        for (Bicycle b : bikes){
            if(bikemap.get(b.getBicycleID()) == null){
                if ((b.getStatus().toLowerCase().equals("rented")) && (b.getCoordinates().getLatitude() != null) && (b.getCoordinates().getLongitude() != null)){
                    MarkerOptions markerOptions = new MarkerOptions()
                            .position(new LatLong(b.getCoordinates().getLatitude(), b.getCoordinates().getLongitude()))
                            .icon("clustered/images/bikeMarker.png");
                    Marker marker = new Marker(markerOptions);
                    map.addMarker(marker);
                    bikemap.put(b.getBicycleID(), marker);

                    map.addUIEventHandler(marker, UIEventType.click, (e) ->{
                        InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
                        infoWindowOptions.content("<h5>Information<h5>"
                                +   "Bicycle ID: " + b.getBicycleID()
                                +   "<br>Status: " + b.getStatus()
                                +   "<br>Current location: Lat - " + formatter.format(b.getCoordinates().getLatitude()) + "  Long - " + formatter.format(b.getCoordinates().getLongitude()) + "<br>"
                                +   "<br>Tripcount: " + b.getTripCount() + "    Distance travelled: " + b.getDistanceTravelled());
                        InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
                        bikeWindow.open(map,marker);
                    });
                }
            }
        }
    }

    /**
     * Checks whether a bike is close to a docking station and docks it if necessary
     */
    public void dock() {
        for (DockingStation d : dockings) {
                double dLat = d.getCoordinates().getLatitude();
                double dLon = d.getCoordinates().getLongitude();
                for (Bicycle b : bikes) {
                    double bLat = b.getCoordinates().getLatitude();
                    double bLon = b.getCoordinates().getLongitude();
                    if (bLat > dLat - 0.0001 || bLat < dLat + 0.0001 || bLon > dLon - 0.0001 || bLon < dLon + 0.0001) {
                        b.setStatus("Docked");
                        bicycleDAO.setMakeorStatus(b.getBicycleID(), "Docked", 1);
                        b.setCoordinates(d.getCoordinates());
                        b.incrementTripCount(1);
                        slotDAO.addBicycleToSlot(b, d);
                    }
                }

        }
    }

    public void updateDocks(){
        updateDockList();
    }

    public void updateBikes() {
        updateBikeList();
        for (int i = 0; i < bikes.size(); i++){
            if (!bikes.get(i).getStatus().toLowerCase().equals("rented")
                    && bikes.get(i).getCoordinates().getLatitude() != null
                    && bikes.get(i).getCoordinates().getLongitude() != null
                    && bikemap.get(bikes.get(i).getBicycleID()) != null) {
                map.removeMarker(bikemap.get(bikes.get(i).getBicycleID()));
                bikemap.remove(bikes.get(i).getBicycleID());
            }

            if (bikes.get(i).getStatus().toLowerCase().equals("rented")
                    && bikes.get(i).getCoordinates().getLatitude() != null
                    && bikes.get(i).getCoordinates().getLongitude() != null
                    && bikemap.get(bikes.get(i).getBicycleID()) != null) {

                Marker edit = bikemap.get(bikes.get(i).getBicycleID());
                bikemap.remove(bikes.get(i).getBicycleID());
                edit.setPosition(new LatLong(bikes.get(i).getCoordinates().getLatitude(), bikes.get(i).getCoordinates().getLongitude()));
                bikemap.put(bikes.get(i).getBicycleID(), edit);
            }
        }
    }

    /**
     * Goes through the list of bikes and docking stations and updates those necessary (position, battery, etc)
     */
    public void updateElements(){
        System.out.println("from elements************************************************");
        updateDocks();
        updateBikes();
    }
    


    public void addBikeList(Bicycle bike){
        System.out.println(bike);
        list.add(bike);
    }

    /**
     * Changes view in the scene to the home-tab
     */
    @FXML
    public void goTo_home(){
        box_home.setVisible(true);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #2A5058;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the bicycle-tab
     */
    @FXML
    public void goTo_bicycles(){
        box_home.setVisible(false);
        box_bicycles.setVisible(true);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #2A5058;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the docking station-tab
     */
    @FXML
    public void goTo_dockingStations(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(true);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #2A5058;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the settings-tab
     */
    @FXML
    public void goTo_settings(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(true);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #2A5058;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the overview- and simulation - map-tab
     */
    @FXML
    public void goTo_map(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(true);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #2A5058;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the bicycle settings tab
     */
    @FXML
    public void goTo_bicycleSettings(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(true);
        box_statistics.setVisible(false);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #2A5058;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the statistics tab
     */
    @FXML
    public void goTo_statistics(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(true);
        box_repairs.setVisible(false);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #2A5058;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #61a2b1;");
    }

    /**
     * Changes view in the scene to the repairs-tab
     */
    @FXML
    public void goTo_repairs(){
        box_home.setVisible(false);
        box_bicycles.setVisible(false);
        box_dockingStations.setVisible(false);
        box_settings.setVisible(false);
        box_map.setVisible(false);
        box_bicycleSettings.setVisible(false);
        box_statistics.setVisible(false);
        box_repairs.setVisible(true);
        btnHome.setStyle("-fx-background-color: #61a2b1;");
        btnBicycles.setStyle("-fx-background-color: #61a2b1;");
        btnDockingStations.setStyle("-fx-background-color: #61a2b1;");
        btnMap.setStyle("-fx-background-color: #61a2b1;");
        btnBicycleSettings.setStyle("-fx-background-color: #61a2b1;");
        btnStatistics.setStyle("-fx-background-color: #61a2b1;");
        btnSettings.setStyle("-fx-background-color: #61a2b1;");
        btnRepairs.setStyle("-fx-background-color: #2A5058;");
    }

    /**
     * Opens new window for adding a bicycle
     */
    @FXML
    private void goTo_addBicycle(){
        try {

            //Parent root = FXMLLoader.load(getClass().getResource("add_bicycle.fxml"));

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("add_bicycle.fxml"));
            AnchorPane pane = loader.load();

            Controller_addBicycle cab = loader.getController();
            if(cab != null){
                cab.setCommunicator(getCommunciator());
            }
            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }

    }

    /**
     * Opens a new window for editing the selected bicycle
     * @param bike  the bicycle to be edited
     */
    @FXML
    private void goTo_editBicycle(Bicycle bike){
        try {

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("edit_bicycle.fxml"));
            AnchorPane pane = loader.load();
            Controller_editBicycle ceb = loader.getController();

            if(bike != null){
                ceb.setBike(bike);
            }

            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Opens a new window for adding a docking station
     */
    @FXML
    public void goTo_addDockingStation(){
        try {
            FXMLLoader loader = new FXMLLoader(Main.class.getResource("add_dockingStation.fxml"));
            AnchorPane pane = loader.load();

            Controller_dockingStation cos = loader.getController();
            if(cos != null){
                cos.setCommunciator(getCommunciator());
            }

            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

            stage.setOnHiding(event -> {

                if(newDocking){
                    updateDockList();
                    updateDocks();
                }
                if (updatedocking){
                    Marker edit = dockmap.get(editDockID);
                    dockmap.remove(editDockID);
                    edit.setPosition(new LatLong(editDockingLat, editDockingLong));
                    dockmap.put(editDockID, edit);
                }
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    static void setNewDockingStation(){
        newDocking = true;
    }

    static void setEditDockingStation(double latitude, double longitude, String newName, int newCap, int dockID){
        editDockingLat = latitude;
        editDockingLong = longitude;
        editDockCapacity = newCap;
        editDockName = newName;
        editDockID = dockID;
        updatedocking = true;
    }

    /**
     * Opens a new window for editing a selected docking station
     * @param dock  the docking station to be edited
     */
    @FXML
    private void goTo_editDockingStation(DockingStation dock){
        try {

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("edit_dockingStation.fxml"));
            AnchorPane pane = loader.load();
            Controller_dockingStation cds = loader.getController();

            cds.setDockingStation(dock);
            cds.setCommunciator(getCommunciator());
            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Deletes one or more selected bicycles
     * @param index the selected bicycle(s) to be deleted
     */
    @FXML
    private void deleteBicycle(ObservableList<Integer> index){
        owner = bikeList.getScene().getWindow();
        BicycleDAO bikeDAO = new BicycleDAO();
        for(int indexofbike:index){
            if(bikeDAO.deleteBicycle(list.get(indexofbike).getBicycleID()) < 0){
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not delete bike: " + list.get(indexofbike).getBicycleID() );
                continue;
            }
        }
        list.removeAll(bikeList.getSelectionModel().getSelectedItems());
    }

    /**
     * Deletes one or more docking stations
     * @param docks the selected docking station(s) to be deleted
     */
    @FXML
    private void deleteDockingStations(ObservableList<DockingStation> docks){
        System.out.println(docks);
        owner = dockingList.getScene().getWindow();
        DockingStationDAO dockDAO = new DockingStationDAO();
        for(DockingStation dockingS:docks){
            if(dockDAO.deleteDockingStation(dockingS.getDockingID()) < 0){
                map.removeMarker(dockmap.get(dockingS.getDockingID()));
                dockmap.remove(dockingS.getDockingID());
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not delete dockingstation: " + dockingS.getDockingID());
                continue;
            }
        }
        dList.removeAll(dockingList.getSelectionModel().getSelectedItems());
    }

    /**
     * Logs out by going back to the login-page
     * @throws IOException in case the adminLogin.fxml can't be loaded
     */
    @FXML
    public void logOut() throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("adminLogin.fxml"));
        Stage stage = (Stage) box_home.getScene().getWindow();
        Scene scene = new Scene(root, 1280, 720);
        stage.setScene(scene);
        stage.show();
    }


    /**
     * Method for getting the communicator bound to this class
     * @return Communicator object
     */
    private Communciator getCommunciator(){
        return new Communciator() {
            Bicycle bike2 = new Bicycle();

            @Override
            public Bicycle getBike(){
                return bike;
            }

            @Override
            public void addBikeToBikeList(Bicycle bike) {
                list.add(bike);
            }

            @Override
            public void addDockingToDockList(DockingStation dock) {
                dList.add(dock);

            }

            @Override
            public void addRepairToList(Reparation request) {
                bikeRepairsList.add(request);
                repairBikeList.refresh();
            }

            @Override
            public void refreshTable() {
                dockingList.refresh();
            }

            @Override
            public void getStage(Stage stage) {
                goTo_map();
                mapset(stage);
            }

            @Override
            public GoogleMap getMap() {
                goTo_map();
                return map;
            }

            @Override
            public void gotToDock() {
                goTo_dockingStations();
            }

        };
    }

    public void mapset(Stage stage){
        this.stage = stage;
    }

    public void chooseCoordinates(Stage stage) {
        Double lat = latLong.getLatitude();
        Double lon = latLong.getLongitude();

        System.out.println(lat + " " + lon);

        getcoords(lat, lon);
        goTo_dockingStations();
        stage.show();
        this.stage = null;

        //cancelMap();

    }

    /**
     * Sets up comminicator interface that allows communicating with other classes to this one
     */
    public interface Communciator{
        Bicycle getBike();
        void addBikeToBikeList(Bicycle bike);
        void addDockingToDockList(DockingStation dock);
        void addRepairToList(Reparation request);
        void refreshTable();
        void getStage(Stage stage);
        GoogleMap getMap();
        void gotToDock();
    }
    
    /**
     * Method changes the email to a logged in administrator
     * @see   LogIn
     * @param event on event of pressing button
     */
    @FXML
    public void changeEmail(ActionEvent event){

        Thread thread = new Thread(()->{

            Window owner = changeButton.getScene().getWindow();
            String oldmail = oldmailTextfield.getText();
            String newmail = newemailTextfield.getText();
            String newmailRepeat = newmailTextfieldRepeat.getText();
            System.out.println(event.getSource());

            AdministratorDAO adminDAO = new AdministratorDAO();
            int id = getAdminId(oldmail);
            if(id < 0){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "The old email you entered is not valid. Try again.");
                    oldmailTextfield.clear();
                    changeButton.setDisable(true);
                    //return;

                });
            }else if(!validEmailFormat(newmail)){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "The email you entered is not of the right format\n Valid format: 'youremail@something.com'");
                    newemailTextfield.clear();
                    changeButton.setDisable(true);
                    //return;

                });
            }else if(oldmail.trim().compareTo(newmailRepeat.trim()) != 0){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "Your new email is not the same in the two new email fields. Try again.");
                    newmailTextfieldRepeat.clear();
                    changeButton.setDisable(true);
                    //return;

                });
            }else{
                if(adminDAO.editAdminEmail(oldmail, newmail)){
                    Platform.runLater(()->{
                        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Changed email",
                                "You successfully changed your email to " + newmail);

                    });
                }
            }
            Platform.runLater(()->{
                oldmailTextfield.clear();
                newemailTextfield.clear();
                newmailTextfieldRepeat.clear();
                changeButton.setDisable(true);

            });
        });
        thread.start();
    }
    
    /**
     * Method changes password to a logged in administrator
     * @see   LogIn
     * @param event on event of pressing button
     */
    @FXML
    public void changePassword(ActionEvent event){

        Thread thread = new Thread(()->{
            Window owner = changePasswordButton.getScene().getWindow();
            String oldPassword = oldPasswordTextField.getText();
            String newPassword = newPasswordTextField.getText();
            String newPasswordRepeat = newPasswordTextFieldRepeat.getText();
            System.out.println(event.getSource());

            AdministratorDAO admin = getCurrAdmin();
            int id = admin.getAdminId();
            System.out.println("Admin id = " + id);
            if(!PasswordManager.validatePassword(oldPassword, admin.getHashFromDB(id))){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "The old password you entered is not valid. Try again.");
                    oldPasswordTextField.clear();
                    changePasswordButton.setDisable(true);

                });
            }else if(newPassword.length() < 8){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "The password you entered is not of the right format.\n"
                                    + "Valid format must contain minimum 8 characters.");
                    newPasswordTextField.clear();
                    changePasswordButton.setDisable(true);

                });
            }else if(newPassword.trim().compareTo(newPasswordRepeat.trim()) != 0){
                Platform.runLater(()->{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error",
                            "Your new password is not the same in the two new password fields. Try again.");
                    newPasswordTextFieldRepeat.clear();
                    changeButton.setDisable(true);

                });
            }else{
                if(LogIn.changePword(oldPassword, newPassword)){
                    Platform.runLater(()->AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Changed email",
                            "You successfully changed your email to " + newPassword));
                    changePasswordButton.setDisable(true);
                    newPasswordTextField.clear();
                    oldPasswordTextField.clear();
                    newPasswordTextFieldRepeat.clear();
                }else {
                    Platform.runLater(()->{
                        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Form error",
                                "something went wrong. Please try again.");
                    });
                }
            }
        });
        thread.start();
    }

    @FXML @SuppressWarnings("Duplicates")
    public void handleKeyReleased(){
        String field1 = oldmailTextfield.getText();
        String field2 = newemailTextfield.getText();
        String field3 = newmailTextfieldRepeat.getText();
        boolean disableButtons = (field1.isEmpty() || field1.trim().isEmpty())
                || (field2.isEmpty() || field2.trim().isEmpty()) || (field3.isEmpty() || field3.trim().isEmpty());

        changeButton.setDisable(disableButtons);
    }

    @FXML @SuppressWarnings("Duplicates")
    public void handleKeyReleasedChangeP(){
        String old = oldPasswordTextField.getText();
        String field2 = newPasswordTextField.getText();
        String field3 = newPasswordTextFieldRepeat.getText();
        boolean disableButtons = (old.isEmpty() || old.trim().isEmpty())
                || (field2.isEmpty() || field2.trim().isEmpty()) || (field3.isEmpty() || field3.trim().isEmpty());
        changePasswordButton.setDisable(disableButtons);
    }


    /**
     * Metod opens page for finalising repairs.
     * @param rep Reparation object of the repair you wish to finalize
     */
    @FXML @SuppressWarnings("Duplicates")
    private void goTo_registerRep(Reparation rep){
        try {
            //Parent root = FXMLLoader.load(getClass().getResource("registerRepair.fxml"));

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("registerRepair.fxml"));
            AnchorPane pane = loader.load();

            Controller_repairs cr = loader.getController();
            if(cr != null){
                cr.setCommunicator(getCommunciator());
                cr.setRepair(rep);
            }

            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method that opens page that allows you to register repairs required on a bicycle
     * @param bike The bicycle you wish to registers repairs required to
     */
    @FXML
    private void goTo_registerRepReq(Bicycle bike){
        try {
            //Parent root = FXMLLoader.load(getClass().getResource("registerRepairRequest.fxml"));

            FXMLLoader loader = new FXMLLoader(Main.class.getResource("registerRepairRequest.fxml"));
            AnchorPane pane = loader.load();

            Controller_repairs cr = loader.getController();
            if(cr != null){
                cr.setCommunicator(getCommunciator());
                cr.setBike(bike);
            }

            Scene scene = new Scene(pane, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Method that opens page that allows you to add another administrator
     */
    @FXML
    public void goTo_addAdmin(){
        try {
            Parent root = FXMLLoader.load(getClass().getResource("add_administrator.fxml"));

            Scene scene = new Scene(root, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

        }
        catch (Exception e){
            e.printStackTrace();
        }
    }
    
    /**
     * Edits a selected type's price.
     * @param type the selected type
     */
    public void editTypePrice(Type type){
        int price = Integer.parseInt(newTypeTextField.getText());
        TypeDAO typeDAO = new TypeDAO();
        if(typeDAO.editPrice(type.getTypeID(), price) > 0){
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Changes saved", "Price edited");
        }else{
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not edit price");
            return;
        }
        type.setPrice(price);
        typeTable.refresh();
    }
    
    /**
     * Deletes a selected type
     * @param index the index of the selected type
     */
    public void deleteType(ObservableList<Integer> index){
        owner = bikeList.getScene().getWindow();
        TypeDAO typeDAO = new TypeDAO();
        for(int indexofbike:index){
            if(typeDAO.deleteType(typeList.get(indexofbike).getTypeID()) < 0){
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not delete type: " + list.get(indexofbike).getBicycleID() );
                continue;
            }
        }
        AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Changes saved", "Types deleted:");
        typeList.removeAll(typeTable.getSelectionModel().getSelectedItems());
    }

}