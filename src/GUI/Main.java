package GUI;

import Simulator.ScheduledExecutorSimulation;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.IOException;

 /**
 * Main class where application starts from. Launches graphical user interface
 * and simulation on two separate threads.
 */

public class Main extends Application {

    public static void main(String[] args) {
        ScheduledExecutorSimulation scheduledExecutorSimulation = new ScheduledExecutorSimulation();
        
        Thread simulation = new Thread(()-> {
            try {
                scheduledExecutorSimulation.simulate();
        
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        });
        
        Thread program = new Thread(()-> launch(args));
        
        program.start();
        simulation.start();
    }
    
    /**
     * Overrides start method and is launched in main above. Starts graphical user interface.
     * @param stage The main window of the program when startup is launched.
     * @throws IOException when something is not loaded correctly.
     */
    @Override
    public void start(Stage stage) throws IOException {
        Parent root = FXMLLoader.load(getClass().getResource("adminLogin.fxml")); 

        Scene scene = new Scene(root, 1280, 720 );

        stage.setTitle("Trondheim bicycle rental system");
        stage.setScene(scene);
        stage.show();
        
    }
}