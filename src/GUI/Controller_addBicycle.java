package GUI;

/**
 * Class for adding bicycle with GUI
 */

import GUI.Controller.Communciator;
import data.Bicycle;
import data.Type;
import db.BicycleDAO;
import db.TypeDAO;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.swing.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.*;

public class Controller_addBicycle implements Initializable {

    public Button btnAddCancel;
    public ComboBox<Type> typeComboBox;
    public Label addBicyclePriceLabel;
    public TextField makeTextField;
    public DatePicker regDate;
    private ObservableList<Bicycle> bikeList = FXCollections.observableArrayList();

    private Controller cont = new Controller();

    private Communciator communicator;


    public Controller_addBicycle(){}

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        //TODO: Add this to own thread
        TypeDAO typeDAO = new TypeDAO();
        ArrayList<Type> types = typeDAO.getAllType();
        ObservableList<Type> typeList = FXCollections.observableArrayList(types);
        typeComboBox.getItems().addAll(typeList);
        typeComboBox.valueProperty().addListener(new ChangeListener<Type>() {
            @Override
            public void changed(ObservableValue<? extends Type> observable, Type oldValue, Type newValue) {
                addBicyclePriceLabel.setText(String.valueOf(newValue.getPrice()));
            }
        });
        System.out.println("View is now loaded!");
    }


    public void setCommunicator(Communciator communicator){
        this.communicator = communicator;
    }

    /**
     * Exits addBicycleWindow, back to main page
     */
    @FXML
    public void cancel() {
        try {
            Stage stage = (Stage) btnAddCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method for getting info entered into field and converting it to a bicycle object and
     * then adding it to the database and the list og bikes in main controller
     */
    @FXML
    public void addBicycle(){
        Window owner = typeComboBox.getScene().getWindow();
        Bicycle bike;
        LocalDate date = regDate.getValue();
        String text = makeTextField.getText();
        Type newType = typeComboBox.getValue();
        if(text.trim().equalsIgnoreCase("") || text == null){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter a make");
            return;
        }
        if(newType == null){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must choose a type");
            return;
        }
        if(date == null){
            date = LocalDate.now();
            bike = new Bicycle(text,date);
        }else{
            bike = new Bicycle(text, date);
        }

        BicycleDAO bicycleDAO = new BicycleDAO();
        int ret = bicycleDAO.addBicycle(bike,newType.getTypeID());
        if (ret > 0){
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Form error", "Bicycle suceccfully registered, ID: " + ret);
            bike = bicycleDAO.getBicycle(ret);
            cancel();
            cont.addBikeList(bike);
            communicator.addBikeToBikeList(bike);
        }else{
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not register bicycle");
        }
    }

}
