package GUI;
/**
 * Controllers for adding and editing repairs
 */

import data.Bicycle;
import data.Reparation;
import db.RepairDAO;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.stage.Stage;
import javafx.stage.Window;
import java.util.TimerTask;
import java.util.Timer;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import javax.swing.*;
import java.net.URL;
import java.time.LocalDate;
import java.util.ResourceBundle;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class Controller_repairs implements Initializable {

    public Button btnRepCancel;
    public Button btnRepReqCancel;
    private Bicycle bike;
    public DatePicker repReqDate;
    public TextArea repReqDesTextArea;
    public DatePicker registerRepairDate;
    public TextField repPriceTextField;
    public TextArea repDesTextArea;

    private Reparation repair;
    private Controller cont = new Controller();

    private Controller.Communciator communicator;
    Timer t = new Timer();


    public Controller_repairs(){}

    public void setCommunicator(Controller.Communciator communicator){
        this.communicator = communicator;
    }

    /**
     * Sets the bicycle that the repiars request is to be added to
     * @param bike
     */
    public void setBike(Bicycle bike){
        this.bike = bike;
    }

    /**
     * Sets the Reparation object of the repair to be finalized
     * @param repair
     */
    public void setRepair(Reparation repair){
        this.repair = repair;
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("View is now loaded!");
    }

    /**
     * Method for registering repairs that is required on a bicycle using GUI
     */
    @FXML
    public void registerRepairReq(){
        Window owner = repReqDate.getScene().getWindow();
        RepairDAO repDAO = new RepairDAO();
        String repReq = repReqDesTextArea.getText();
        LocalDate repDate = repReqDate.getValue();
        if(repReq == null || repReq.trim() == ""){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter required repairs");
        }
        if(repDate == null){
            repDate = LocalDate.now();
        }

        Reparation repairs = new Reparation(repDate, repReq);
        int ret = repDAO.registerRepairs(repairs, bike.getBicycleID());
        if(ret < 0){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not register repair request");
        }else{
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Form error", "Repair request registered, request ID: " + ret);
            bike.setStatus("Repair");
        }
        repairs = new Reparation(ret, repDate, null, repReq, 0, null);
        communicator.addRepairToList(repairs);
        cancelRepReq();


    }


    /**
     * Metod for registering repairs done to a bicycle using GUI
     */
    @FXML
    public void registerRepair(){
        Window owner = repDesTextArea.getScene().getWindow();
        //todo: method for registering repairs
        RepairDAO repDAO = new RepairDAO();
        LocalDate date = registerRepairDate.getValue();
        if(date == null){
            date = LocalDate.now();
        }
        int price = Integer.parseInt(repPriceTextField.getText());
        String repDone = repDesTextArea.getText();

        if(repDone == null || repDone.trim().equalsIgnoreCase("")){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter a description of repairs done");
            return;
        }

        int ret = repDAO.returnBike(repair.getRepairID(), date, price, repDone);
        if(ret > 0){
            AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Form error", "Repairs finished");
            repair.setDateReturned(date);
            repair.setPrice(price);
            repair.setRepairdesc(repDone);
            cancelRep();
        }else{
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Repairs could not be registered");
            cancelRep();
        }
        //communicator.refreshRepair();
        cancelRep();

    }

    @FXML
    public void cancelRep() {
        try {
            Stage stage = (Stage) btnRepCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @FXML
    public void cancelRepReq() {
        try {
            Stage stage = (Stage) btnRepReqCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }




}


