package GUI;

/**
 * Class for adding and editing a docking station
 */

import GUI.Controller.Communciator;
import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import data.Coordinate;
import data.DockingStation;
import db.DockingStationDAO;
import db.SlotDAO;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.Window;

import javax.swing.*;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import static GUI.Controller.setNewDockingStation;
import static GUI.Controller.setEditDockingStation;

public class Controller_dockingStation implements Initializable, MapComponentInitializedListener {

    private DecimalFormat formatter = new DecimalFormat("###.00000");
    private static Double longd;
    private static Double lati;

    public Label addDockingStationLatitudeLabel;
    public Label addDockingStationLongitudeLabel;
    //public TextField addDockingStationLongitudeText;
    //public TextField addDockingStationLatitudeText;
    public Button btnAddDockingStationMap;
    public Button btnAddDockingStationCancel;
    public Button btnAddDockingStationMapCancel;
    public Button btnEditDockingStationCancel;
    public TextField dockName;
    public TextField dockCapacity;

    private DockingStation dockingStation = new DockingStation();
    public TextField editDockingStationLongitudeText;
    public TextField editDockingStationLatitudeText;
    public TextField editDockName;
    public TextField editDockCapacity;
    public Label editDockIdLabel;
    private Communciator communciator;

    @FXML
    private GoogleMapView mapView;
    @FXML
    private GoogleMap map;
    private LatLong latLong;
    private Marker newStation;

    public Controller_dockingStation(){};

    public void setCommunciator(Communciator communciator){
        this.communciator = communciator;
    }

    public void setDockingStation(DockingStation dock){
        System.out.println(editDockIdLabel);
        this.dockingStation = dock;
        editDockIdLabel.setText(String.valueOf(dock.getDockingID()));
        editDockingStationLatitudeText.setText(String.valueOf(dock.getCoordinates().getLatitude()));
        editDockingStationLongitudeText.setText(String.valueOf(dock.getCoordinates().getLongitude()));
        editDockName.setText(dock.getName());
        editDockCapacity.setText(String.valueOf(dock.getCapacity()));
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        System.out.println("Add docking view is now loaded!");
    }

    public void mapInitialized(){ }

    @FXML
    private void setCoordinates() {
        /*String currentLat = String.valueOf(lati);
        String currentLong = String.valueOf(longd);*/
        if(addDockingStationLatitudeLabel != null && addDockingStationLongitudeLabel != null){
            addDockingStationLatitudeLabel.setText(formatter.format(lati));
            addDockingStationLongitudeLabel.setText(formatter.format(longd));
        }
        if(editDockingStationLatitudeText != null && editDockingStationLongitudeText != null){
            editDockingStationLatitudeText.setText(formatter.format(lati));
            editDockingStationLongitudeText.setText(formatter.format(longd));
        }
    }

    public static void getcoords(Double lat, Double lon){
        lati = lat;
        longd = lon;

        System.out.println(lati);
        System.out.println(longd);

    }

    public void goTo_addDockingStationMap(){
        try {
            System.out.println("Cancel");
            Stage stage = null;
            if(btnAddDockingStationCancel != null){
                stage = (Stage) btnAddDockingStationCancel.getScene().getWindow();
            }else{
                stage = (Stage) btnEditDockingStationCancel.getScene().getWindow();
            }
            stage.hide();
            communciator.getStage(stage);
            //map = communciator.getMap();
            //setCoord(stage, communciator);
            stage.setOnShowing(event -> {
                System.out.println("Map window exited");
                if (longd != null && lati != null) setCoordinates();
                //setMapReloadBool();
            });


            //stage.show();
        }catch(Exception e){
            e.printStackTrace();
        }
        /*try {
            Parent root = FXMLLoader.load(getClass().getResource("addDockingStationMap.fxml"));

            Scene scene = new Scene(root, 600, 400 );
            Stage stage = new Stage();
            stage.setTitle("Trondheim bicycle rental system");
            stage.setScene(scene);
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.show();

            stage.setOnHiding(event -> {
                System.out.println("Map window exited");
                if (longd != null && lati != null) setCoordinates();
                setMapReloadBool();
            });
        }
        catch (Exception e){
            e.printStackTrace();
        }*/
    }

    public void setCoord(Stage stage, Communciator communciator){
        map.clearMarkers();
        System.out.println("3");
        map.clearMarkers();

    }

    /**
     * Method for adding a docking station to database using GUI
     *
     */
    public void addDockingStation(){
        Window owner = dockName.getScene().getWindow();
        String name = dockName.getText();
        Integer capacity = Integer.parseInt(dockCapacity.getText());

        double latitude = lati; //Double.parseDouble(addDockingStationLatitudeText.getText());
        double longitude = longd; //Double.parseDouble(addDockingStationLongitudeText.getText());

        Coordinate coordinate = new Coordinate(latitude,longitude);
        DockingStation dock = new DockingStation(name, coordinate);

        Coordinate coordinaten = new Coordinate(lati,longd);
        DockingStation dock1 = new DockingStation(name, coordinaten);

        if(name.trim().equalsIgnoreCase("") || name == null){
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "You must enter a name");
            return;
        }


        DockingStationDAO dockDao = new DockingStationDAO();
        SlotDAO slotDAO = new SlotDAO();
        int ret = dockDao.addDockingStation(dock);
        int slotRet = 0;
        setNewDockingStation();

        System.out.println(ret);


        if (ret > 0){
            System.out.println(ret);
            Alert alert = new Alert(Alert.AlertType.INFORMATION, "Dockingstation suceccfully registered, ID: " + ret);
            alert.showAndWait();

            for(int i = 0; i<capacity;i++){
                slotRet = slotDAO.addSlot(ret);
            }
            if(slotRet < 0){
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not add slots to dockingstation");
            }
            dock = dockDao.getDockingStation(ret);
            communciator.addDockingToDockList(dock);
            cancelAdd();
            //cont.addBikeList(bike);
        }else{
            AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not register dockingstation");
        }
    }

    /**
     * Method for canceling adding docking station and returning to docking station page in main controller
     */
    public void cancelAdd(){
        try {
            System.out.println("Cancel");
            Stage stage = (Stage) btnAddDockingStationCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Method for editing a docking station already in database using GUI
     */
    @SuppressWarnings("Duplicates")
    public void editDockingStation(){
        Window owner = btnEditDockingStationCancel.getScene().getWindow();
        String name = editDockName.getText();
        Integer capacity = Integer.parseInt(editDockCapacity.getText());

        Coordinate coordinate = new Coordinate(longd,lati);
        //DockingStation dock = new DockingStation(name, coordinate);

        DockingStationDAO dockDao = new DockingStationDAO();
        SlotDAO slotDAO = new SlotDAO();

        if(name != null && !name.trim().equalsIgnoreCase("") && name != dockingStation.getName()){
            if(dockDao.editDockingStation(dockingStation.getDockingID(), name) > 0){
                dockingStation.setName(name);
            }else{
                AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not set new name");
            }
        }
        if(lati != null && longd != null){
            if(lati != dockingStation.getCoordinates().getLatitude() || longd != dockingStation.getCoordinates().getLongitude()){
                if(dockDao.editDockingStation(dockingStation.getDockingID(), lati, longd) > 0){
                    dockingStation.getCoordinates().setLatitude(lati);
                    dockingStation.getCoordinates().setLongitude(longd);
                }else{
                    AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Could not set new coordinates");
                }
            }
        }
        System.out.println(capacity);
        System.out.println(dockingStation.getCapacity());

        if(capacity != dockingStation.getCapacity()){
            if(capacity < dockingStation.getCapacity()){
                for(int i = capacity; i < dockingStation.getCapacity(); i++){
                    slotDAO.deleteSlot(dockingStation.getDockingID());
                }
                dockingStation.setCapacity(capacity);
            }else if(capacity > dockingStation.getCapacity()){
                for (int i = dockingStation.getCapacity(); i < capacity; i++){
                    slotDAO.addSlot(dockingStation.getDockingID());
                }
                dockingStation.setCapacity(capacity);
            }
        }

        setEditDockingStation(dockingStation.getCoordinates().getLatitude(), dockingStation.getCoordinates().getLongitude(), dockingStation.getName(), dockingStation.getCapacity(), dockingStation.getDockingID());
        System.out.println(communciator);
        communciator.refreshTable();
        cancelEditDockingStation();

    }

    /**
     * Method for canceling editing dockingstation and returning to docking station page on main page
     */
    public void cancelEditDockingStation(){
        try {
            Stage stage = (Stage) btnEditDockingStationCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}