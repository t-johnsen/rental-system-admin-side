package GUI;

import db.AdministratorDAO;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.stage.Window;
import util.LogIn;

import java.net.URL;
import java.util.ResourceBundle;

public class Controller_addAdmin implements Initializable{
	
	@FXML
	private Button btnCancelAddAdmin;
	@FXML
	private TextField newAdminEmail;
	@FXML
	private Button btnAddAdmin;
	
	@Override
	public void initialize(URL location, ResourceBundle resources){
		btnAddAdmin.setDisable(false);
	}
	
	@FXML
	public void addAdmin(){
		Thread t = new Thread(()->{
			Window owner = btnAddAdmin.getScene().getWindow();
			String mail = newAdminEmail.getText();
			
			if(!AdministratorDAO.validEmailFormat(mail)){
				Platform.runLater(()->{
					AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "The email has wrong format. Please try again.");
					newAdminEmail.clear();
					btnAddAdmin.setDisable(true);
				});
			}else{
				if(LogIn.newAdmin(mail)){
					Platform.runLater(()->{
						AlertHelper.showAlert(Alert.AlertType.INFORMATION, owner, "Confirmation", mail + "\nis now added as an administrator.\n\n Password is sent to email.");
					});
				}else {
					Platform.runLater(()->{
						AlertHelper.showAlert(Alert.AlertType.ERROR, owner, "Form error", "Something went wrong when adding new administrator,\n or this email is already registered.");
					});
				}
			}
			
		});
		t.start();
		cancel();
	}
	
	@FXML
	public void cancel(){
		try {
			Stage stage = (Stage) btnCancelAddAdmin.getScene().getWindow();
			stage.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@FXML
	public void handleKeyReleaseAdmin(){
		String field1 = newAdminEmail.getText();
		boolean disableButtons = field1.isEmpty() || field1.trim().isEmpty();
		btnAddAdmin.setDisable(disableButtons);
	}
	
	
	
}
