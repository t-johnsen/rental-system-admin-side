package GUI;

import com.lynden.gmapsfx.GoogleMapView;
import com.lynden.gmapsfx.MapComponentInitializedListener;
import com.lynden.gmapsfx.javascript.event.GMapMouseEvent;
import com.lynden.gmapsfx.javascript.event.UIEventType;
import com.lynden.gmapsfx.javascript.object.*;
import com.lynden.gmapsfx.util.MarkerImageFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.stage.Stage;

import java.net.URL;
import java.text.DecimalFormat;
import java.util.ResourceBundle;

import static GUI.Controller_dockingStation.getcoords;


public class Controller_dockingStationMap implements Initializable, MapComponentInitializedListener {
    public Button btnAddDockingStationMapCancel;
    private DecimalFormat formatter = new DecimalFormat("###.00000");
    @FXML
    private GoogleMapView mapView;
    @FXML
    private GoogleMap map;
    private LatLong latLong;
    private Marker newStation;
    /*String path = "GUI/icons/DSMarker.png";
    String imgpath = MarkerImageFactory.createMarkerImage(path, "png");*/

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        mapView.addMapInializedListener(this);
    }

    @Override
    public void mapInitialized() {
        MapOptions mapOptions = new MapOptions();
        mapOptions.center(new LatLong(63.429067, 10.392625))
                .mapType(MapTypeIdEnum.ROADMAP)
                .overviewMapControl(false)
                .panControl(false)
                .rotateControl(false)
                .scaleControl(false)
                .streetViewControl(false)
                .zoomControl(true)
                .zoom(13)
                .minZoom(11)
                .maxZoom(20)
                .mapTypeControl(false);

        map = mapView.createMap(mapOptions);

        map.addMouseEventHandler(UIEventType.click, (GMapMouseEvent event) -> {
            latLong = event.getLatLong();
            if (newStation != null) map.removeMarker(newStation);
            if (latLong.getLongitude() > 10.5 || latLong.getLongitude() < 10.3 || latLong.getLatitude() < 63.4 || latLong.getLatitude() > 63.5) {
                AlertHelper.showAlert(Alert.AlertType.ERROR, mapView.getScene().getWindow(), "Error", "Selected coordinates cannot be outside of Trondheim");
                return;
            }
            newStation = new Marker(new MarkerOptions()
                    .position(latLong)
                    .animation(Animation.BOUNCE));
            map.addMarker(newStation);
            InfoWindowOptions infoWindowOptions = new InfoWindowOptions();
            infoWindowOptions.content("<h5>Chosen coordinates<h5>"
                    + "Current location: Lat - " + formatter.format(latLong.getLatitude()) + "  Long - " + formatter.format(latLong.getLongitude()) + "<br>");
            InfoWindow bikeWindow = new InfoWindow(infoWindowOptions);
            map.addUIEventHandler(newStation, UIEventType.click, (e) -> {
                bikeWindow.open(map, newStation);
            });
        });
    }

    public void chooseCoordinates() {
        Double lat = latLong.getLatitude();
        Double lon = latLong.getLongitude();
        System.out.println(lat);
        System.out.println(lon);


        getcoords(lat, lon);

        cancelMap();

    }

    public void cancelMap(){
        try {
            Stage stage = (Stage) btnAddDockingStationMapCancel.getScene().getWindow();
            stage.close();
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
