package Simulator;

import data.Bicycle;
import data.Coordinate;
import data.DockingStation;
import db.BicycleDAO;
import db.DatabaseConnection;
import db.DockingStationDAO;

import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static util.PasswordManager.generateTempPassword;

/**
 * This class simulates different types of states a bicycle and dockingstation can have.
 * The purpose of this class is to simulate like there would be real bicycles ringing
 * around and sending information to database. This class will replicate that.
 */

public class Simulation implements Runnable{
    private final double MAX_LAT = 63.435000;
    private final double MIN_LAT = 63.423040;
    private final double MAX_LONG = 10.412175;
    private final double MIN_LONG = 10.366513;
    private final double METERSPERMINUTE = 100.0;
    private final int MAX_BATTERY = 100;
    private final int MIN_BATTERY = 0;
    private final Coordinate REPAIR = new Coordinate(59.946028, 10.765223);
    
    private BicycleDAO bicycleDAO;
    private DockingStationDAO dockDAO;
    private ArrayList<Bicycle> bikes;
    private ArrayList<DockingStation> dockingStations;
    
    public Simulation(){
        cleanSlotsWhenStartup();
        dockDAO = new DockingStationDAO();
        bicycleDAO = new BicycleDAO();
        bikes = bicycleDAO.getAllBicycles();
        dockingStations = dockDAO.getAllDockingStations();
        for(Bicycle bike : bikes){
            simulateNewRoute(bike, makeRandomStart());
        }
        
    }
    
    /**
     * Method overrides run method and runs for specific intervals.
     * @see Simulator.ScheduledExecutorSimulation
     */
    @Override
    public void run(){
        System.out.println("-----------IS SIMULATING NOW-----------");
        updateBikes();
        updateDockings();
        
        simulateDistance();
    }
    
    /**
     * Method simulates movement of bicycles, rentals, repairs, docking to dockingstation
     * and saves simulated data to database to use in program.
     * The bicycle's status decides what simulation which will happen depending on a number
     * of factors.
     */
    private void simulateDistance(){
        
        for(Bicycle bike : bikes){
    
            switch (bike.getBiiikesStatus()){
                case docked:
                    
                    if(bike.getSimulatedTimeDocked() > 0) {
                        simulateWhenDocked(bike);
                    }else if((bike.getSimulatedTimeDocked() <= 0) && (bike.getChargingLvl() > 40)) {
                        simulateNewRoute(bike, bike.getDestinationDocking().getCoordinates());
                        simulateWhenRent(bike);
                    }else if((bike.getSimulatedTimeDocked() <= 0) && (bike.getChargingLvl() < 40)){
                        bike.setSimulatedDockTime(ThreadLocalRandom.current().nextInt(2, 6));
                    }
                    break;
                
                case rented:

                    if(((bike.getStepCount() >= (bike.getRouteCoordinates().length-1)))) {
                        simulateWhenDocking(bike);
                    }else {
                        simulateWhenRiding(bike);
                    }
                    break;
                
                case repair:
                    
                    if(bike.getTimeRepair() > 0){
                        bike.decreaseTimeRepair();
                    }else if(bike.getTimeRepair() == 0){
                        simulateFromRepair(bike);
                    }else if(bike.getTimeRepair() == -1){
                        bike.setTimeRepair(ThreadLocalRandom.current().nextInt(10, 15));
                        bike.setCoordinates(REPAIR);
                    }
                    break;
                
                default:
                    simulateWhenDocking(bike);
                    break;
            }
            System.out.println("**************************BIKE STATUS: " + bike.getBiiikesStatus());
    
        }
        
    }
    
    /**
     * Simulates when bicycle is back from repair and docks to a random dockingstation with
     * enough capacity.
     * @param bike Bicycle from arraylist of bikes updates bicycle information.
     */
    private void simulateFromRepair(Bicycle bike){
        DockingStation tempDock = null;
        for(DockingStation dock : dockingStations){
            if(dock.getCapacity() > 1){
                tempDock = dock;
                break;
            }
        }
        bike.setBiiikesStatus(Bicycle.BikeStatus.docked);
        
        bike.setDestinationDocking(tempDock);
        bike.setTimeRepair(-1);
        bike.setStepCount(-1);
        bike.setCoordinates(bike.getDestinationDocking().getCoordinates());
        bike.setIsDockedIn(bike.getDestinationDocking().getDockingID());
        bike.setSimulatedDockTime(ThreadLocalRandom.current().nextInt(8, 15));
        
    }
    
    /**
     * Simulates when a bicycle is docking to a station.
     * @param bike Bicycle from arraylist of bikes updates bicycle information.
     */
    private void simulateWhenDocking(Bicycle bike){
        DockingStation tempDock = bike.getDestinationDocking();
        if(tempDock.getCapacity() == 0){
            simulateNewRoute(bike, tempDock.getCoordinates());
        }else {
            bike.setStepCount(-1);
            bike.setTimeRepair(-1);
            bike.setBiiikesStatus(Bicycle.BikeStatus.docked);
            
            bike.increaseTripByOne();
            bike.setCoordinates(tempDock.getCoordinates());
            bike.setIsDockedIn(tempDock.getDockingID());
            bike.increaseDistanceTravelled(bike.getDistanceToBeTravelled());
            bike.setSimulatedDockTime(ThreadLocalRandom.current().nextInt(2, 6));
    
        }
    }
    
    /**
     * Simulates when a bicycle is docked in station.
     * @param bike Bicycle from arraylist of bikes updates bicycle information.
     */
    private void simulateWhenDocked(Bicycle bike){
        bike.decreaseTimeDocked();
        if(bike.getChargingLvl() > 94){
            bike.setChargingLvl(MAX_BATTERY);
        }else {
            bike.increaseChargingLevel();
        }
    }
    
    /**
     * Simulates when a bicycle is rented from user and writes this to database.
     * @param bike Bicycle from arraylist of bikes updates bicycle information.
     */
    private void simulateWhenRent(Bicycle bike){

        bike.setBiiikesStatus(Bicycle.BikeStatus.rented);
        bike.setSimulatedDockTime(-1);
        bike.setTimeRepair(-1);
        
        String sqlQ = "INSERT INTO Rental (rental_id, bank_info, fine, bike_id, dateRented) VALUES " +
                "(DEFAULT ,?,?,?,?)";
        String randBankInfo = generateTempPassword(); //Just for simulation purposes
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            
            try(PreparedStatement prepStmt = con.prepareStatement(sqlQ)){
                con.setAutoCommit(false);
                prepStmt.setString(1, randBankInfo);
                prepStmt.setInt(2, 0);
                prepStmt.setInt(3, bike.getBicycleID());
                LocalDate date = LocalDate.now();
                prepStmt.setDate(4, Date.valueOf(date));
                con.commit();
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
            }finally {
                con.setAutoCommit(true);
            }
            
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Simulates when a bicycle is riding.
     * @param bike Bicycle from arraylist of bikes updates bicycle information.
     */
    private void simulateWhenRiding(Bicycle bike){
        int index = bike.getStepCount();
        
        if(bike.getChargingLvl() <= MIN_BATTERY){
            bike.setChargingLvl(MIN_BATTERY);
        }else {
            bike.decreaseChargingLevel();
        }
        
        if(index < bike.getRouteCoordinates().length) {
            bike.setCoordinates(bike.getRouteCoordinates()[index]);
            bike.increaseStepCount();
        }
    }
    
    /**
     * Gives the bicycle a new route for simulation purposes.
     * @param bike       Bicycle from arraylist of bikes updates bicycle information.
     * @param startcoord the start coordinate of the bike.
     */
    private void simulateNewRoute(Bicycle bike, Coordinate startcoord){
        DockingStation station = randomDock(dockingStations);
        
        if(bike.getDestinationDocking() != null){
            while (bike.getDestinationDocking().equals(station)) {
                station = randomDock(dockingStations);
            }
        }
        
        if(bike.getChargingLvl() < 40){
            bike.setToBeDockedInID(station.getDockingID());
            bike.setDestinationDocking(station);
            simulateWhenDocking(bike);
        
        }else {
            Coordinate[] tempCoords = generateRoute(startcoord, station.getCoordinates());
            bike.setDestinationDocking(station);
            if (bike.getChargingLvl() <= MIN_BATTERY) {
                simulateWhenDocking(bike);
            }
            bike.setBiiikesStatus(Bicycle.BikeStatus.rented);
            bike.setSimulatedDockTime(-1);
            bike.setTimeRepair(-1);
            bike.setRouteCoordinates(tempCoords);
            bike.setCoordinates(tempCoords[0]);
            bike.setToBeDockedInID(station.getDockingID());
            bike.setStepCount(0);
            bike.setDistanceToBeTravelled(distanceTravelled(tempCoords[0], tempCoords[tempCoords.length - 1], 'K'));
        }
    }
    
    /**
     * Generates a route with vector calculation.
     * @param start Coordinate which bicycle will start from.
     * @param stop  Coordinate which bicycle will stop.
     * @return      Coordinate[] array filled with coordinates - the bicycles steps.
     */
    private Coordinate[] generateRoute(Coordinate start, Coordinate stop){
        int steps = generateSteps(start, stop);
        Coordinate[] route = new Coordinate[steps];
        if(route.length < 1){
            generateRoute(start, stop);
        }
        route[0] = start;
        double vectorLat = stop.getLatitude() - start.getLatitude();
        double vectorLong = stop.getLongitude() - start.getLongitude();
        double vectorStepLat = vectorLat/steps;
        double vectorStepLong = vectorLong/steps;
        
        for(int i = 1; i < steps; i++){
            route[i] = new Coordinate(
                    route[i-1].getLatitude() + vectorStepLat, route[i-1].getLongitude() + vectorStepLong);
            System.out.println("Making coords: " + i + ": " + route[i].toString());
        }
        
        return route;
        
    }
    
    /**
     * Updates local arraylist of bicycles from database for simulation purposes.
     */
    private void updateBikes(){
        List<Bicycle> tempBikes = bicycleDAO.getAllBicycles();
        for(Bicycle tempBike : tempBikes) {
            bikes.sort(new BicycleComparator());
        
            if (!(bikes.contains(tempBike))) {
                bikes.add(tempBike);
            }
            
            for(Bicycle bike : bikes) {
                if(!(tempBikes.contains(bike))){
                    bikes.remove(bike);
                }
                if ((bike.equals(tempBike)) && (tempBike.getBiiikesStatus() == bike.getBiiikesStatus())) {
                    bike.setBiiikesStatus(tempBike.getBiiikesStatus());
                }
        
            }
        }
        
    }
    
    /**
     * Updates local arraylist of dockingstation from database for simulation purposes.
     */
    private void updateDockings(){
        List<DockingStation> tempDocks = dockDAO.getAllDockingStations();
        for(DockingStation tempDock : tempDocks){
            dockingStations.sort(new DockingComparator());
            
            if(!(dockingStations.contains(tempDock))){
                dockingStations.add(tempDock);
            }
            
            for(DockingStation dock : dockingStations){
                if(!(tempDocks.contains(dock))){
                    dockingStations.remove(dock);
                }
                if((dock.equals(tempDock)) && (dock.getCapacity() != tempDock.getCapacity())){
                    dock.setCapacity(tempDock.getCapacity());
                }
                dock.setPowerUsage(simulatePowerUsage(tempDock));
            }
        }
        
    }
    
    /**
     * Method which simulates powerusage in a dockinstation.
     * @param dock from local arraylist of DockingStations.
     * @return     int which is used for simulation purposes.
     */
    private int simulatePowerUsage(DockingStation dock){
        int capacity = dock.getCapacity();
        if(capacity > 25){
            return 30;
        }else if(capacity < 15 && capacity > 7){
            return 50;
        }else if(capacity < 7){
            return 70;
        }else {
            return 10;
        }
    }
    
    /**
     * Method for starting simulation on startup of program.
     */
    private void cleanSlotsWhenStartup(){
        String sqlQuerySlot = "UPDATE Slot SET bike_id = NULL;";
        try(Connection con = DatabaseConnection.getInstance().getConnection()
        ){
            try(PreparedStatement premtStmt = con.prepareStatement(sqlQuerySlot)){
                con.setAutoCommit(false);
                int affected = premtStmt.executeUpdate();
                
                con.commit();
                
                if(affected >= 0){
                    System.out.println("Cleaned up slots for startup!");
                }
                
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
            }finally {
                con.setAutoCommit(true);
            }
        
        }catch (SQLException e){
            e.printStackTrace();
        }
    }
    
    /**
     * Generates a random Coordinate which is used to calculate fictional route.
     * @return Coordinate
     */
    private Coordinate makeRandomStart(){
        double latitude = MIN_LAT + (Math.random())* (MAX_LAT - MIN_LAT);
        double longitude = MIN_LONG + (Math.random())* (MAX_LONG - MIN_LONG);
        return new Coordinate(latitude, longitude);
    }
    
    /**
     * Finds a random dockingstation from arraylist for simulation purposes.
     * @param randDock Arraylist with current dockingstations.
     * @return DockingStaion
     */
    private DockingStation randomDock(ArrayList<DockingStation> randDock){
        int randNum = (int)(Math.random() * ((randDock.size()-1) + 1));
        return randDock.get(randNum);
    }
    
    
    public ArrayList<Bicycle> getSimulatedBikes(){
        return bikes;
    }
    
    public ArrayList<DockingStation> getSimulatedDockings(){
        return dockingStations;
    }
    
    /**
     * Generates a number of steps depending on distance of route.
     * @param start Coordinate
     * @param stop  Coordinate
     * @return      int which is used for generating a vector route for simulation purposes.
     */
    private int generateSteps(Coordinate start, Coordinate stop){ //WORKING
        double km = distanceTravelled(start, stop, 'K');
        double m = km * 1000;
        return (int) (m / METERSPERMINUTE);
    }
    
    /**
     * A mathematical method for calculating length from one coordinate to another.
     * @param coor1 Coordinate
     * @param coor2 Coordinate
     * @param unit  char whcich is used to decide between kilometers, miles or nautical miles
     * @return      double of distance
     */
    private double distanceTravelled(Coordinate coor1, Coordinate coor2, char unit) {
        double theta = coor1.getLongitude() - coor2.getLongitude();
        double dist = Math.sin(deg2rad(coor1.getLatitude())) * Math.sin(deg2rad(coor2.getLatitude())) + Math.cos(deg2rad(coor1.getLatitude())) * Math.cos(deg2rad(coor2.getLatitude())) * Math.cos(deg2rad(theta));
        dist = Math.acos(dist);
        dist = rad2deg(dist);
        dist = dist * 60 * 1.1515;
        if (unit == 'K') {
            dist = dist * 1.609344;
        } else if (unit == 'N') {
            dist = dist * 0.8684;
        }
        return (dist);
    }
    
    /**
     * Calculates degrees to radians.
     * @param deg double variable degrees.
     * @return    double in radians.
     */
    private double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
    
    /**
     * Calculates radians to degrees.
     * @param rad double variable radians.
     * @return    double in degrees.
     */
    private double rad2deg(double rad) {
        return (rad * 180.0 / Math.PI);
    }
    
}

/**
 * BicycleComparator is used to sort Arraylist of bicycles after the ID of all bicycles.
 */
class BicycleComparator implements Comparator<Bicycle> {
    public int compare(Bicycle bike1, Bicycle bike2) {
        return bike1.getBicycleID() - bike2.getBicycleID();
    }
}

/**
 * BicycleComparator is used to sort Arraylist of dockingstations after the ID of all dockingstations.
 */
class DockingComparator implements Comparator<DockingStation> {
    public int compare(DockingStation dock1, DockingStation dock2) {
        return dock1.getDockingID() - dock2.getDockingID();
    }
}
