package Simulator;

import data.DockingStation;
import db.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Writes updates from simulation of dockingstations to database and register
 * all data and updates.
 * This class has a overridden run() method which is scheduled to run from
 * @see ScheduledExecutorSimulation
 */


public class WritingDockingstations implements Runnable{
    
    
    ArrayList<DockingStation> docks;
    Simulation simulation;
    
    public WritingDockingstations(Simulation sim){
        simulation = sim;
    }
    
    /**
     * This method is called from
     * @see ScheduledExecutorSimulation when startup of program.
     */
    @Override
    public void run(){
        docks = simulation.getSimulatedDockings();
        insertSimulatedDockings(docks);
    }
    
    private boolean insertSimulatedDockings(ArrayList<DockingStation> dockings){
        
        String sqlQuery = "UPDATE Dockingstation SET name = ?, dock_longitude = ?, dock_latitude = ?, power_used = ? WHERE dock_id = ?";
        boolean ok = false;
        
        try(Connection con = DatabaseConnection.getInstance().getConnection()
        ){
            try(PreparedStatement prepStmt = con.prepareStatement(sqlQuery)){
                con.setAutoCommit(false);
                
                for(DockingStation dock : dockings){
                    prepStmt.setString(1, dock.getName());
                    prepStmt.setDouble(2, dock.getCoordinates().getLongitude());
                    prepStmt.setDouble(3, dock.getCoordinates().getLatitude());
                    prepStmt.setInt(4, dock.getPowerUsage());
                    prepStmt.setInt(5, dock.getDockingID());
                    prepStmt.addBatch();
                }
                int[] count = prepStmt.executeBatch();
                con.commit();
                ok = true;
                for(int num : count){
                    System.out.println("From WritingDockingstations: " + num);
                }
                
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
                ok = false;
            }finally {
                con.setAutoCommit(true);
            }
            
        }catch (SQLException e){
            e.printStackTrace();
        }
        return ok;
        
    }
    
    
}
