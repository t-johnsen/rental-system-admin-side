package Simulator;

import data.Bicycle;
import db.DatabaseConnection;

import java.sql.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;

/**
 * Writes updates from simulation of coordinates to database and register
 * all data and positions a bicycle have had and current location.
 * This class has a overridden run() method which is scheduled to run from
 * @see ScheduledExecutorSimulation
 */


public class WritingCoordinates implements Runnable{
    
    ArrayList<Bicycle> bikes;
    Simulation simulation;
    
    public WritingCoordinates(Simulation sim){
        simulation = sim;
    }
    
    /**
     * This method is called from
     * @see ScheduledExecutorSimulation when startup of program.
     */
    @Override
    public void run(){
        bikes = simulation.getSimulatedBikes();
        insertSimulatedCoordinates(bikes);
    }
    
    private boolean insertSimulatedCoordinates(ArrayList<Bicycle> bikes){
        
        String sqlQuery = "INSERT INTO Storing_coordinates (bike_id, coord_date, coord_time, coord_longitude, coord_latitude) " +
                "VALUES (?,?,?,?,?)";
        boolean ok = false;
        
        try(Connection con = DatabaseConnection.getInstance().getConnection()
        ){
            try(PreparedStatement prepStmt = con.prepareStatement(sqlQuery)){
                con.setAutoCommit(false);
                
                for(Bicycle bike : bikes){
                    LocalDate date = LocalDate.now();
                    prepStmt.setInt(1, bike.getBicycleID());
                    prepStmt.setDate(2, Date.valueOf(date));
                    prepStmt.setTime(3, Time.valueOf(LocalTime.now()));
                    prepStmt.setDouble(4, bike.getCoordinates().getLongitude());
                    prepStmt.setDouble(5, bike.getCoordinates().getLatitude());
                    prepStmt.addBatch();
                }
                int[] count = prepStmt.executeBatch();
                con.commit();
                ok = true;
                for(int num : count){
                    System.out.println("From WritingCoordinates: " + num);
                }
                
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
                ok = false;
            }finally {
                con.setAutoCommit(true);
            }
            
        }catch (SQLException e){
            e.printStackTrace();
        }
        return ok;
    
    }
    
}
