package Simulator;

import data.Bicycle;
import db.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;

/**
 * Writes updates from simulation of bicycles which has docked to a slot on dockingstation
 * to database and register all data and updates.
 * This class has a overridden run() method which is scheduled to run from
 * @see ScheduledExecutorSimulation
 */

public class WritingSlots implements Runnable{
    
    private Simulation simulation;
    private ArrayList<Bicycle> bicycles;
    
    public WritingSlots(Simulation sim){
        simulation = sim;
    }
    
    /**
     * This method is called from
     * @see ScheduledExecutorSimulation when startup of program.
     */
    @Override
    public void run(){
        bicycles = simulation.getSimulatedBikes();
        insertSimulatedSlots(bicycles);
        removeFromSimulatedSlots(bicycles);
    }
    
    
    private boolean insertSimulatedSlots(ArrayList<Bicycle> bikes){
        String sqlQuerySlot = "UPDATE Slot SET bike_id = ? WHERE dock_id = ? AND bike_id IS NULL LIMIT 1";
        boolean ok;
        try(Connection con = DatabaseConnection.getInstance().getConnection()
        ){
            
            try(PreparedStatement premtStmt = con.prepareStatement(sqlQuerySlot)){
                con.setAutoCommit(false);
                
                for(Bicycle bike : bikes){
                    String status = bike.getBiiikesStatus().toString();
                    if((status.compareToIgnoreCase("docked") == 0) && !bike.getHasSlot()){
                        bike.setHasSlot(true);
                        premtStmt.setInt(1, bike.getBicycleID());
                        premtStmt.setInt(2, bike.getToBeDockedInID());
                        premtStmt.addBatch();
                    }
                }
                
                int[] countSlotsUpdated = premtStmt.executeBatch();
                con.commit();
                ok = true;
                for(int num : countSlotsUpdated){
                    System.out.println("Updated slots: " + num);
                }
                
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
                ok = false;
            }finally {
                con.setAutoCommit(true);
            }
            
        }catch (SQLException e){
            e.printStackTrace();
            ok = false;
        }
        return ok;
    }
    private boolean removeFromSimulatedSlots(ArrayList<Bicycle> bikes){
        String sqlQueryRemove = "UPDATE Slot SET bike_id = ? WHERE dock_id = ? AND bike_id = ?";
        boolean ok;
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
    
            try(PreparedStatement prepStmnt = con.prepareStatement(sqlQueryRemove)){
                con.setAutoCommit(false);
        
                for(Bicycle bike : bikes){
                    String status = bike.getBiiikesStatus().toString();
                    if((status.compareToIgnoreCase("rented") == 0) && bike.getHasSlot()) {
                        bike.setHasSlot(false);
                        prepStmnt.setNull(1, Types.INTEGER);
                        prepStmnt.setInt(2, bike.getToBeDockedInID());
                        prepStmnt.setInt(3, bike.getBicycleID());
                        prepStmnt.addBatch();
                    }
                }
                int[] updatesSlots = prepStmnt.executeBatch();
                con.commit();
                ok = true;
                for(int num : updatesSlots){
                    System.out.println("REMOVED SLOTS: " + num);
                }
        
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
                ok = false;
            }finally {
                con.setAutoCommit(true);
            }
        }catch (SQLException e){
            e.printStackTrace();
            ok = false;
        }
        return ok;
    }
    
}
