package Simulator;

import data.Bicycle;
import db.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.ArrayList;


/**
 * Writes updates from simulation of bicycles to database and register
 * all simulated statuses. This class has a overridden run() method
 * which is scheduled to run from
 * @see ScheduledExecutorSimulation
 */

public class WritingBicycleStatus implements Runnable{
    
    private ArrayList<Bicycle> bikes;
    private Simulation simulation;
    private int counter = 0;
    
    public WritingBicycleStatus(Simulation sim){
        simulation = sim;
    }
    
    /**
     * This method is called from
     * @see ScheduledExecutorSimulation when startup of program.
     */
    @Override
    public void run(){
        counter++;
        bikes = simulation.getSimulatedBikes();
        insertSimulatedStatuses(bikes);
    }
    
    private boolean insertSimulatedStatuses(ArrayList<Bicycle> bikes){
        String sqlQueryBicycle = "UPDATE Bicycle SET status = ?, battery_percent = ?, trips = ?, distanceTravelled = ?" +
                " WHERE bike_id = ?";
        boolean ok = false;
    
        try(Connection con = DatabaseConnection.getInstance().getConnection()
        ){
            try(PreparedStatement prepStmt = con.prepareStatement(sqlQueryBicycle)){
                con.setAutoCommit(false);
            
                for(Bicycle bike : bikes){
                    String status = bike.getBiiikesStatus().toString();
                    prepStmt.setString(1, status);
                    prepStmt.setInt(2, bike.getChargingLvl());
                    prepStmt.setInt(3, bike.getTripCount());
                    prepStmt.setDouble(4, bike.getDistanceTravelled());
                    prepStmt.setInt(5, bike.getBicycleID());

                    prepStmt.addBatch();
                    
                }
                int[] countBikesUpdated = prepStmt.executeBatch();
                con.commit();
                ok = true;
                for(int num : countBikesUpdated){
                    System.out.println("From WritingBicycleStatus: " + num);
                }
            
            }catch (SQLException e){
                e.printStackTrace();
                con.rollback();
                ok = false;
            }finally {
                con.setAutoCommit(true);
            }
            
        
        }catch (SQLException e){
            e.printStackTrace();
        }
        return ok;
    }
}
