package Simulator;

import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * This class has the responsibility of scheduling threads to the different
 * simulating classes including the classes which writes to database.
 */

public class ScheduledExecutorSimulation {
    private Simulation sim;
    private WritingBicycleStatus writingBikes;
    private WritingCoordinates writingCoords;
    private WritingDockingstations writingDocks;
    private WritingSlots writingSlots;
    
    
    public ScheduledExecutorSimulation(){
        sim = new Simulation();
        writingBikes = new WritingBicycleStatus(sim);
        writingCoords = new WritingCoordinates(sim);
        writingDocks = new WritingDockingstations(sim);
        writingSlots = new WritingSlots(sim);
    }
    
    /**
     * Is called from Main when program is launched.
     * @throws InterruptedException if thread is interrupted.
     */
    public void simulate() throws InterruptedException{
        init(sim, writingBikes, writingCoords, writingDocks, writingSlots);
    }
    
/*    public static void main(String[] args) throws InterruptedException{
        Simulation sim = new Simulation();
        WritingBicycleStatus writingBikes = new WritingBicycleStatus(sim);
        WritingCoordinates writingCoords = new WritingCoordinates(sim);
        WritingDockingstations writingDocks = new WritingDockingstations(sim);
        WritingSlots writingSlots = new WritingSlots(sim);
        init(sim, writingBikes, writingCoords, writingDocks, writingSlots);
    }*/
    
    
    /**
     * Initializes at startup of program and schedules threads to different
     * tasks with specific time intervals and delays.
     * @param sim                    Simulator object
     * @param writingBikesToDB       WritingBicycleStatus object
     * @param writingCoordinates     WritingCoordinates object
     * @param writingDockingstations WritingDockingstations object
     * @param writingSlotsToDB       WritingSlots object
     * @throws InterruptedException  when threads are interrupted
     */
    private static void init(Simulation sim, WritingBicycleStatus writingBikesToDB, WritingCoordinates writingCoordinates, WritingDockingstations writingDockingstations, WritingSlots writingSlotsToDB) throws  InterruptedException{
        ScheduledExecutorService executorService = Executors.newScheduledThreadPool(20);

        
        executorService.scheduleAtFixedRate(sim, 0, 30, TimeUnit.SECONDS); // This will simulate
        executorService.scheduleAtFixedRate(writingCoordinates, 7, 30, TimeUnit.SECONDS); //This will write to db
        executorService.scheduleAtFixedRate(writingDockingstations, 10, 30, TimeUnit.SECONDS); //This will write to db
        executorService.scheduleAtFixedRate(writingSlotsToDB, 15, 30, TimeUnit.SECONDS); //This will write to db
        executorService.scheduleAtFixedRate(writingBikesToDB, 20, 30, TimeUnit.SECONDS); //This will write to db
        
        
        TimeUnit.SECONDS.sleep(1000);
        executorService.shutdown();
    }
}














