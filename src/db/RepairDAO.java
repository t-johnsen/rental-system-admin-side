package db;
/**
 * Class that acts as a link to repair table in database
 */

import data.Reparation;

import javax.xml.crypto.Data;
import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;


public class RepairDAO {
    private PreparedStatement ps1;
    private PreparedStatement ps2;
    private ResultSet rs;
    Connection con; // Cannot be created in try method head because of rollback


    public RepairDAO(){}

    /**
     * Metod for registering repairs on a bike, and making it unavailiable by setting status to "Repair"
     * @param repair An reparation object
     * @param bikeID    ID of bike to be repaired
     * @return  Sucessfull: ID of repair. Unsucessful: -1
     */
    public int registerRepairs(Reparation repair, int bikeID){
    try{
        con = DatabaseConnection.getInstance().getConnection();
       con.setAutoCommit(false);
       ps1 = con.prepareStatement("UPDATE Bicycle SET status = 'Repair' WHERE bike_id = ?");
       ps2 = con.prepareStatement("INSERT INTO Repair (date_sent, repair_req, bike_id) VALUE (?,?,?)", Statement.RETURN_GENERATED_KEYS);
       ps1.setInt(1,bikeID);
       ps1.executeUpdate();
        if(repair.getDateSent() == null){
            LocalDate date = LocalDate.now();
            ps2.setDate(1, Date.valueOf(date));
        }else{
            ps2.setDate(1, Date.valueOf(repair.getDateSent()));
        }
       ps2.setString(2,repair.getRepairReqDesc());
       ps2.setInt(3,bikeID);
       ps2.executeUpdate();
       con.commit();
       rs = ps2.getGeneratedKeys();
       if (rs.next()){
           return rs.getInt(1);
       }
    }catch (SQLException e){
        e.printStackTrace();
        if(con != null){
            try{
                con.rollback();
                return -1;
            }catch (SQLException f){
                e.printStackTrace();
            }
        }
    }finally {
        DatabaseConnection.close(ps1);
        DatabaseConnection.close(ps2);
        try{
            con.setAutoCommit(true);
            con.close();
        }catch (SQLException cl){
            cl.printStackTrace();
        }
    }
    return -1;
    }

    /**
     * Completes repair on bike, fillng out the rest of information needed.
     * @param repairID      ID of repair to ne finalized
     * @param date          Date bike is returned
     * @param price         Price of repaird
     * @param repairDone    What repairs were done
     * @return              Nr. rows affected if sucessfull
     *                      -1 if unsucessfull
     * NOTE: It is inteded that status will automatically be set to docked once bike is plugged back into dockingstation
     *        so changing status here is not needed.
     */
    public int returnBike(int repairID, LocalDate date, int price, String repairDone){
        try(Connection con = DatabaseConnection.getInstance().getConnection()) {
            if(date == null){
                date = LocalDate.now();
            }
            ps1 = con.prepareStatement("UPDATE Repair SET date_recived = ?, rep_price = ?, repairs_done = ? WHERE repair_id = ?");
            ps1.setDate(1,Date.valueOf(date));
            ps1.setInt(2,price);
            ps1.setString(3,repairDone);
            ps1.setInt(4,repairID);
            int ret = ps1.executeUpdate();
            return ret;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps1);
        }
        return -1;
    }


    /**
     * Metod for getting all repairs registered to a bicycle
     * @param   bikeID ID of bicycle to get all repairs registered to
     * @return  ArrayList of Reparation object. null is unsucessfull
     */
    public ArrayList<Reparation> getAllRepairs(int bikeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps1 = con.prepareStatement("SELECT * FROM Repair WHERE bike_id = ?");
            ps1.setInt(1,bikeID);
            rs = ps1.executeQuery();

            ArrayList<Reparation> repairs = new ArrayList<>();
            while(rs.next()){
                Reparation rep = new Reparation();
                rep.setRepairID(rs.getInt("repair_id"));
                rep.setDateSent(rs.getDate("date_sent").toLocalDate());
                if (rs.getDate("date_recived") == null){
                    rep.setDateReturned(null);
                }else{rep.setDateReturned(rs.getDate("date_recived").toLocalDate());}
                rep.setPrice(rs.getInt("rep_price"));
                rep.setRepairdesc(rs.getString("repairs_done"));
                rep.setRepairReqDesc(rs.getString("repair_req"));
                repairs.add(rep);
            }
            return  repairs;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps1);
            DatabaseConnection.close(rs);
        }
        return null;
    }

}
