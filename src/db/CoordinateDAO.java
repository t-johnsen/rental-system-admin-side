package db;

import data.Coordinate;

import javax.xml.crypto.Data;
import java.sql.*;

//TODO: Make method that gets all coordinates of a specific bike
public class CoordinateDAO {
    private PreparedStatement ps;
    private ResultSet rs;

    public CoordinateDAO(){
    }

    /**
     * Metod for regiserting coordinates to a bicycle
     * @param coord Coordinate object
     * @param bikeID ID of the bike to register coordinates to
     */
    public void addCoordinates(Coordinate coord, int bikeID){
       try(Connection con = DatabaseConnection.getInstance().getConnection()){
           ps = con.prepareStatement("INSERT INTO Storing_coordinates"+
                   "(bike_id, coord_date, coord_time, coord_longitude, coord_latitude)" +
           "VALUES(?,?,?,?,?)");

           ps.setInt(1,bikeID);
           ps.setDate(2,Date.valueOf(coord.getDate()));
           ps.setTime(3,Time.valueOf(coord.getTime()));
           ps.setDouble(4,coord.getLongitude());
           ps.setDouble(5,coord.getLatitude());

           ps.executeUpdate();
       }catch (SQLException e){
           e.printStackTrace();
       }finally {
           DatabaseConnection.close(ps);
       }
    }

    /**
     * Gets latest coordinates registered to a bike
     * @param bikeID ID of bicycle that you wish to get coordinates for
     * @return  Coordinate object if sucessfull. null if unsucesfull
     */
    public Coordinate getCoordinates(int bikeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT * FROM `Storing_coordinates` AS a WHERE coord_date = (SELECT MAX(coord_date) FROM `Storing_coordinates` AS b WHERE a.bike_id = b.bike_id) AND bike_id = ? ORDER BY `coord_time` DESC LIMIT 1");
            ps.setInt(1,bikeID);
            rs = ps.executeQuery();

            Coordinate coord = new Coordinate();
            if(rs.next()){
                coord.setLatitude(rs.getDouble("coord_latitude"));
                coord.setLongitude(rs.getDouble("coord_longitude"));
                coord.setDate(rs.getDate("coord_date").toLocalDate());
                coord.setTime(rs.getTime("coord_time").toLocalTime());
            }
            return coord;
        }catch (SQLException e){e.printStackTrace();}
        finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return null;
    }


}
