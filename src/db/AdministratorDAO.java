package db;

import data.Administrator;
import util.PasswordManager;

import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The AdministratorDAO class is the layer between
 * @see Administrator and the database.
 * All reading and writing to database including changing from GUI
 * goes through this class.
 */

public class AdministratorDAO{
	private Administrator admin;
	
	
	public AdministratorDAO(){}
	public AdministratorDAO(Administrator admin){
		this.admin = admin;
	}
	
	/**
	 * Adds new administrator to database.
	 * @param email the new email to the new administrator.
	 * @return true if successful.
	 */
	public static boolean addAdministrator(String email){
		boolean ok = false;
		if(!validEmailFormat(email)) return false;
		
		String sqlQ = "INSERT INTO Administrator (admin_id, email, hashedPassword) " +
						"VALUES (DEFAULT,?,?)";
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement prepStmt = con.prepareStatement(sqlQ)
			){
			
			String pass = PasswordManager.generateTempPassword();
			
			
			prepStmt.setString(1, email);
			prepStmt.setString(2, PasswordManager.hashPassword(pass));
			
			int affected = prepStmt.executeUpdate();
			
			if(affected > 0){
				ok = sendEmail(email, pass);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
			ok = false;
		}
		return ok;
	}
	
	/**
	 * Deletes selected admin from database.
	 * @param id the ID number of the selected admin to be deleted.
	 * @return true if successfully deleted.
	 */
	public boolean deleteAdmin(int id){
		boolean ok = false;
		
		String sqlQ = "DELETE FROM Administrator WHERE admin_id = ?";
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement delStmt = con.prepareStatement(sqlQ)){
			
			delStmt.setInt(1, id);
			int affected = delStmt.executeUpdate();
			
			if(affected > 0) ok = true;
			
		}catch(SQLException e){
			e.printStackTrace();
			ok = false;
		}
		
		return ok;
	}
	
	/**
	 * @param username of selected administrator.
	 * @return the ID to the belonging administrator username/email.
	 */
	public static int getAdminId(String username){
		int output = -1;
		String sqlQ = "SELECT admin_id FROM Administrator WHERE email = ?";
		ResultSet resSet = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()
			){
			try(PreparedStatement getStmt = con.prepareStatement(sqlQ)){
				con.setReadOnly(true);
				
				getStmt.setString(1, username);
				resSet = getStmt.executeQuery();
				
				if(resSet.next()){
					output = resSet.getInt(1);
				}
			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				con.setReadOnly(false);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
			
		}finally{
			DatabaseConnection.close(resSet);
			
		}
		
		return output;
	}
	
	/**
	 * @param id of selected administrator.
	 * @return the email to the belonging administrators ID.
	 */
	public String getAdminUsername(int id){
		String output = null;
		String sqlQ = "SELECT email FROM Administrator WHERE admin_id = ?";
		ResultSet resSet = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()
			){
			try(PreparedStatement getStmt = con.prepareStatement(sqlQ)){
				con.setReadOnly(true);
				
				getStmt.setInt(1, id);
				resSet = getStmt.executeQuery();
				
				if(resSet.next()){
					output = resSet.getString(1);
				}
				
			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				con.setReadOnly(false);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resSet);
		}
		
		return output;
	}
	
	/**
	 * Gets hash from database belonging to the administrators ID.
	 * @param id of selected administrator.
	 * @return the hashed password from database.
	 */
	public String getHashFromDB(int id){
		String output = null;
		String sqlQ = "SELECT hashedPassword FROM Administrator WHERE admin_id = ?";
		ResultSet resSet = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()
			){
			try(PreparedStatement pStmt = con.prepareStatement(sqlQ)){
				con.setReadOnly(true);
				
				pStmt.setInt(1, id);
				resSet = pStmt.executeQuery();
				
				if(resSet.next()){
					output = resSet.getString(1);
				}
				if(output == null)
					return null;
				
			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				con.setReadOnly(false);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resSet);
		}
		
		return output;
	}
	
	/**
	 * Edits email to administrator when logged in and writes to database.
	 * @param oldEmail the old email of administrator.
	 * @param newEmail the new email of administrator.
	 * @return true if email is successfully changed. False if not.
	 */
	public boolean editAdminEmail(String oldEmail, String newEmail){
		boolean ok = false;
		ResultSet resset = null;
		String sqlQ = "SELECT * FROM Administrator WHERE admin_id = ?";
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()){
			
			try(PreparedStatement editStmt = con.prepareStatement(sqlQ, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)){
				con.setAutoCommit(false);
				editStmt.setInt(1, getAdminId(oldEmail));
				resset = editStmt.executeQuery();
				
				if(resset.next() && validEmailFormat(newEmail)){
					resset.updateString("email", newEmail);
				}
				resset.updateRow();
				con.commit();
				ok = true;
				
			}catch(SQLException e){
				e.printStackTrace();
				con.rollback();
				ok = false;
			}finally{
				con.setAutoCommit(true);
			}
			//TODO: Test method
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resset);
		}
		return ok;
	}
	
	/**
	 * Method for validating email format. Example of valid email: someName-100@yahoo.com
	 * Example of invalid email: someName123@gmail.a and someName123@.com
	 * @param email The email to validate.
	 * @return		Returns true if email format is valid. If not, returns false.
	 */
	public static boolean validEmailFormat(String email){
		Matcher theMatcher;
		final String format = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
		Pattern pattern = Pattern.compile(format);
		
		theMatcher = pattern.matcher(email);
		
		return theMatcher.matches();
		
	}
	
	
	/**
	 * This method sends the temporary password generated to users email.
	 * @param email Is users current email.
	 * @param pwd Is the temporary generated password.
	 * @return Returns true if email is successfully sent. False if not.
	 */
	public static boolean sendEmail(String email, String pwd){
		final String from = "bicycle.rental.trondheim@gmail.com";
		final String sPwd = "team12HALTER";
		boolean ok;
		final String emailSMTPserver = "smtp.gmail.com";
		final String emailServerPort = "587";
		final String subject = "Temporary login";
		final String msg = "Welcome, new administrator of Trondheim City's electrical bicycle rental system!\n\n"
				+ "Below is your temporary password. Please change this as you log in the first time.\n\n"
				+ "Temporary password: " + pwd;
		
		if(!validEmailFormat(email)) return false;
		
		Properties props = new Properties();
		props.put("mail.smtp.auth", "true");
		props.put("mail.smtp.starttls.enable", "true");
		props.put("mail.smtp.host", emailSMTPserver);
		props.put("mail.smtp.port", emailServerPort);
		
		try{
			Session session = Session.getInstance(props, new javax.mail.Authenticator(){
				protected PasswordAuthentication getPasswordAuthentication(){
					return new PasswordAuthentication(from, sPwd);
				}
			});
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));
			message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(email));
			message.setSubject(subject);
			message.setText(msg);
			Transport.send(message);
			ok = true;
			System.out.println("Email sent.\n");
			
		}catch(Exception e){
			e.printStackTrace();
			ok = false;
		}
		return ok;
	}
	
	public static boolean adminExists(String username){
		
		if(getAdminId(username) < 0) return false;
		return true;
	}
	
	public int getAdminId(){
		return admin.getId();
	}
	
	
	public void setAdminId(int id){
		admin.setId(id);
	}
	
	public void setAdminEmail(String mail){
		admin.setEmail(mail);
	}
	
}
