package db;

import data.Bicycle;
import data.Coordinate;
import data.Type;

import java.sql.*;
import java.time.LocalDate;
import java.util.ArrayList;

public class BicycleDAO {
    private PreparedStatement ps;
    private ResultSet rs;
    private ResultSet rs2;
    private Statement stmt;

    public BicycleDAO(){ }

    /**
     * Method for adding bicycle to the database
     * @param bike: Bicycle object
     * @return If sucessfull: The ID of the registered bicycle. If unsucessfull: -1
     */
    public int addBicycle(Bicycle bike, int typeID){
        try (Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("INSERT INTO Bicycle"+
                    "(status, trips, battery_percent, registered_date, typeID, make, distanceTravelled) "
                    +"VALUES(?,?,?,?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
           ps.setString(1,"Docked");
           ps.setInt(2,0);
           ps.setInt(3,100);
           ps.setDate(4,Date.valueOf(bike.getDate()));
           ps.setInt(5,typeID);   // TODO: Update so it takes in type from Type table // Done?
           ps.setString(6,bike.getMake());
           ps.setInt(7,0);

           int update = ps.executeUpdate();

           // Find way to return generated keys
           if(update == 0){
               return -1;
           }

           rs = ps.getGeneratedKeys();
           if(rs.next()){
               return rs.getInt(1);
           }
        }catch(SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    // TODO: Solve problem if table is locked. (While loop until works)

    /**
     * Method for deleting a bicycle from the database
     * @param bikeID: ID of the bicycle to be deleted
     * @return Integer: 1 if sucessful, -1 if not
     */
    public int deleteBicycle(Integer bikeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("DELETE FROM Bicycle WHERE bike_id = ?");
            ps.setInt(1,bikeID);
            int update = ps.executeUpdate();
            return update;

        }catch (SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

    /**
     * Method for getting a bicycle from database
     * @param bikeID The ID of the bicycle to get
     * @return Bicycle object, null if unsucessfull
     */
    public Bicycle getBicycle(int bikeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT * FROM Bicycle WHERE bike_id = ?");
            ps.setInt(1,bikeID);
            rs = ps.executeQuery();

            Bicycle bike = new Bicycle();
            CoordinateDAO coord = new CoordinateDAO();
            Coordinate cords;
            TypeDAO typeDAO = new TypeDAO();

            if(rs.next()){
                Type type = typeDAO.getType(rs.getInt("typeID"));

                bike.setBicycleID(rs.getInt("bike_id"));
                bike.setChargingLvl(rs.getInt("battery_percent"));
                bike.setPrice(type.getPrice());
                bike.setMake(rs.getString("make"));
                bike.setType(type.getType());
                bike.setDistanceTravelled(rs.getDouble("distanceTravelled"));
                bike.setTripCount(rs.getInt("trips"));
                bike.setDateRegistered(rs.getDate("registered_date").toLocalDate());
                bike.setStatus(rs.getString("status"));
                cords = coord.getCoordinates(bikeID);
                bike.setCoordinates(cords);
            }
            if(bike.getBicycleID() == 0){
                return null;
            }
            return bike;

        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return null;
    }

    /**
     * Gets all bicycles in the database
     * @return ArrayList of Bicycle object. Null if unsuccessful
     */
    public ArrayList<Bicycle> getAllBicycles(){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM Bicycle");
            ArrayList<Bicycle> bikes = new ArrayList<>();

            CoordinateDAO coord = new CoordinateDAO();
            TypeDAO typeDAO = new TypeDAO();

            while(rs.next()){
                Bicycle bike = new Bicycle();
                Coordinate cords;
                Type type = typeDAO.getType(rs.getInt("typeID"));

                bike.setBicycleID(rs.getInt("bike_id"));
                bike.setChargingLvl(rs.getInt("battery_percent"));
                bike.setPrice(type.getPrice());
                bike.setMake(rs.getString("make"));
                bike.setType(type.getType());
                bike.setDistanceTravelled(rs.getDouble("distanceTravelled"));
                bike.setTripCount(rs.getInt("trips"));
                bike.setDateRegistered(rs.getDate("registered_date").toLocalDate());
                bike.setStatus(rs.getString("status"));
                cords = coord.getCoordinates(bike.getBicycleID());
                bike.setCoordinates(cords);
                bikes.add(bike);
            }
            return bikes;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(stmt);
            DatabaseConnection.close(rs);
        }
        return null;
    }

    //TODO: Unused metod. Delete?
    public ArrayList<Bicycle> getBicyclesMarkedForRepair(){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            stmt = con.createStatement();
            rs2 = stmt.executeQuery("SELECT Bicycle.bike_id FROM Bicycle WHERE status LIKE 'repair'");

            ArrayList<Bicycle> bikes = new ArrayList<>();

            while (rs2.next()){
                int bikeID = rs2.getInt("bike_id");
                Bicycle bike = getBicycle(bikeID);
                bikes.add(bike);
            }
            return bikes;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(stmt);
            DatabaseConnection.close(rs2);
        }
        return null;
    }


    //TODO: BETTER METHOD FOR THIS IN DOCKINGDAO, CHANGE?
    /**
     * Method for changing either the status of a bike or the make
     * @param bikeID    ID of bike to be changed
     * @param newInfo   What you want the new field to read (string)
     * @param choice    Int that decides which field to edit
     *                  1 = status
     *                  2 = make
     * @return          Nr. of rows affected if sucessul
     *                  -1 if unsucessful
     */
    public int setMakeorStatus(int bikeID, String newInfo, int choice){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            if(choice == 1){
                ps = con.prepareStatement("UPDATE Bicycle SET status = ? WHERE bike_id = ?");
            }else if(choice == 2){
                ps = con.prepareStatement("UPDATE Bicycle SET make = ? WHERE bike_id = ?");
            }else {
                return -1;
            }
            ps.setString(1,newInfo);
            ps.setInt(2,bikeID);
            int ret = ps.executeUpdate();
            return ret;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

    /**
     * Method for chaning Nr. of trips or distance a bike has travveled
     * @param bikeID    ID of the bike to be altered
     * @param newValue  New value for field (int)
     * @param choice    What field to change
     *                  1 = trips
     *                  2 = distanceTravelled
     *                  3 = typeID
     * @return  Nr. of fields affected if sucessfull.
     *          -1 if unsucessfull
     */
    public int setTripsorDistorType(int bikeID, int newValue, int choice){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            if(choice == 1){
                ps = con.prepareStatement("UPDATE Bicycle SET trips = ? WHERE bike_id = ?");
            }else if(choice == 2){
                ps = con.prepareStatement("UPDATE Bicycle SET distanceTravelled = ? WHERE bike_id = ?");
            }else if(choice == 3){
                ps = con.prepareStatement("UPDATE Bicycle SET typeID = ? WHERE bike_id = ?");
            }else{
                return -1;
            }
            ps.setInt(1,newValue);
            ps.setInt(2,bikeID);
            int ret = ps.executeUpdate();
            return ret;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

    // SEE COMMENT ON setMakeOr----
    public int setBicycleDate(LocalDate date, int bikeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("UPDATE Bicycle SET registered_date = ? WHERE bike_id = ?");
            ps.setDate(1, Date.valueOf(date));
            ps.setInt(2, bikeID);
            int ret = ps.executeUpdate();
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

    }


