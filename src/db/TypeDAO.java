package db;

/**
 * Class that acts as a link to type table in database
 */

import data.Type;

import javax.xml.crypto.Data;
import java.sql.*;
import java.util.ArrayList;

public class TypeDAO {
    private PreparedStatement ps;
    private ResultSet rs;
    private Statement stmt;

    public TypeDAO(){
    }

    /**
     * Adds a new types to the database
     * @param type A type object
     * @return Returns the ID of the object added. -1 if usucessfull
     */
    public int addType(Type type){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("INSERT INTO Type (price, type) VALUES(?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,type.getPrice());
            ps.setString(2,type.getType());

            int typeID = ps.executeUpdate();

            if(typeID == 0){
                return -1;
            }
            rs = ps.getGeneratedKeys();
            if(rs.next()){
                return rs.getInt(1);
            }

        }catch (SQLException e){ e.printStackTrace();}
        finally{
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    /**
     * Gets a specifice bike type from database
     * @param typeID The ID of the bike
     * @return  A bike object. Null if unsucessfull
     */
    public Type getType(int typeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT * FROM Type WHERE typeID = ?");
            ps.setInt(1,typeID);

            rs = ps.executeQuery();
            Type type = new Type();
            if(rs.next()){
                type.setTypeID(rs.getInt("typeID"));
                type.setType(rs.getString("type"));
                type.setPrice(rs.getInt("price"));
            }
            return type;
        }catch (SQLException e){e.printStackTrace();}
        finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
    return null;
    }

    /**
     * Method for getting all types in database
     * @return Arraylist of all types as type objects. Null if unsucessfull
     */
    public ArrayList<Type> getAllType(){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            stmt = con.createStatement();
            rs = stmt.executeQuery("SELECT * FROM Type");
            ArrayList<Type> types = new ArrayList<>();

            while(rs.next()){
                Type type = new Type();

                type.setTypeID(rs.getInt("typeID"));
                type.setPrice(rs.getInt("price"));
                type.setType(rs.getString("type"));
                types.add(type);
            }

            return types;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            // Close connections
            DatabaseConnection.close(stmt);
            DatabaseConnection.close(rs);
        }
        return null;
    }

    /**
     *  Method for editing price of a type of bicycle
     * @param typeID ID of the type you wish to edit the price of
     * @param newPrice New price to be set for the type
     * @return Number of tables altered (should be 1). Returns -1 if unsuccessful
     */
    public int editPrice(int typeID, int newPrice){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("UPDATE Type SET price = ? WHERE typeID = ?");
            ps.setInt(1,newPrice);
            ps.setInt(2,typeID);
            int i = ps.executeUpdate();
            return i;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

    public int deleteType(Integer typeID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("DELETE FROM bragew.Type WHERE typeID = ?");
            ps.setInt(1,typeID);
            int update = ps.executeUpdate();
            return update;

        }catch (SQLException e){
            e.printStackTrace();
        }catch(Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }

}
