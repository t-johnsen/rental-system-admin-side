package db;

import data.Bicycle;

import java.sql.*;
import java.time.LocalDate;

public class RentalDAO {
    private PreparedStatement ps;
    private ResultSet rs;

    public int addRental(int bikeID, LocalDate date, int bankLast, int fine){
        try (Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("INSERT INTO Rental"+
                    "(bank_info, fine, bike_id, dateRented) "
                    +"VALUES(?,?,?,?)", Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,bankLast);
            ps.setInt(2,fine);
            ps.setInt(3, bikeID);
            ps.setDate(4, Date.valueOf(date));

            int update = ps.executeUpdate();

            if(update == 0){
                return -1;
            }

            rs = ps.getGeneratedKeys();
            if(rs.next()){
                return rs.getInt(1);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int getRental(LocalDate date){
        try (Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT COUNT(*) FROM Rental WHERE dateRented = ?");
            ps.setDate(1, Date.valueOf(date));

            rs = ps.executeQuery();




            if (rs.next()){
                return rs.getInt(1);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int getIncome(LocalDate date){
        try (Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT SUM(price)+SUM(fine) FROM Type\n" +
                    "  LEFT JOIN Bicycle\n" +
                    "    on Bicycle.typeID = Type.typeID\n" +
                    "  LEFT JOIN Rental\n" +
                    "    on Bicycle.bike_id = Rental.bike_id\n" +
                    "  WHERE dateRented = ?;");
            ps.setDate(1, Date.valueOf(date));

            rs = ps.executeQuery();




            if (rs.next()){
                return rs.getInt(1);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int getExpenses(LocalDate date){
        try (Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT SUM(rep_price) FROM Repair WHERE date_recived = ?;");
            ps.setDate(1, Date.valueOf(date));

            rs = ps.executeQuery();




            if (rs.next()){
                return rs.getInt(1);
            }
        }catch(SQLException e){
            e.printStackTrace();
        }catch (Exception e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }
}
