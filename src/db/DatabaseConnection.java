package db;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;

/**
 * Class that controls connection with database which uses a pool
 * to get connections from. This means you can have several connections
 * to the database at the same time.
 */

public class DatabaseConnection{
	
	private static DatabaseConnection dataSource;
	private ComboPooledDataSource comboPooledDataSource;
	private Properties prop = new Properties();
	private InputStream inn = null;
	
	public DatabaseConnection(){
		try{
			System.setProperty("com.mchange.v2.log.MLog", "com.mchange.v2.log.FallbackMLog");
			System.setProperty("com.mchange.v2.log.FallbackMLog.DEFAULT_CUTOFF_LEVEL", "WARNING");

			inn = new FileInputStream("config.properties"); // FOR REAL
			//inn = new FileInputStream("configTest.properties"); // FOR TESTING
			prop.load(inn);


			comboPooledDataSource = new ComboPooledDataSource();
			comboPooledDataSource.setDriverClass("com.mysql.jdbc.Driver");
			comboPooledDataSource.setJdbcUrl(prop.getProperty("dbUrl"));
			comboPooledDataSource.setUser(prop.getProperty("dbUsr"));
			comboPooledDataSource.setPassword(prop.getProperty("dbPwd"));

			comboPooledDataSource.setMaxPoolSize(10);
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}


	public static DatabaseConnection getInstance(){
		if(dataSource == null){
			dataSource = new DatabaseConnection();
		}
		return dataSource;
	}
	
	public Connection getConnection(){
		try{
			return comboPooledDataSource.getConnection();
		}catch(SQLException e){
			e.printStackTrace();
		}
		return null;
	}
	
	public void close(){
		try{
			comboPooledDataSource.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void close(PreparedStatement ps){
		try{
			ps.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void close(ResultSet rs){
		try{
			rs.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static void close(Statement stmt){
		try{
			stmt.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public int getCountOfConns(){
        try {
            return comboPooledDataSource.getNumConnections();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return -1;
    }
	
	
}
