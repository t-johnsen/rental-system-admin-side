package db;

import data.Bicycle;
import data.Coordinate;
import data.DockingStation;
import data.Type;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static java.sql.Statement.RETURN_GENERATED_KEYS;

public class DockingStationDAO{
	
	
	public DockingStationDAO(){
	}
	
	/**
	 * Adds a new dockingstation to database.
	 * @param newDockingStation is the new dockingstation to be added.
	 * @return the ID of the new dockingstation if added successfully, and -1 if not.
	 */
	public int addDockingStation(DockingStation newDockingStation){
		String sql = "INSERT INTO Dockingstation "
				+ "(name, dock_longitude, dock_latitude) "
				+ "VALUES (?,?,?)";
		ResultSet resSet = null;
		
		
		ArrayList<DockingStation> ds = getAllDockingStations();
		if(ds == null){
			System.out.println("NULL");
			return -1;
		}
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement pstmt = con.prepareStatement(sql, RETURN_GENERATED_KEYS)
		){
			
			pstmt.setString(1, newDockingStation.getName());
			pstmt.setDouble(2, newDockingStation.getCoordinates().getLongitude());
			pstmt.setDouble(3, newDockingStation.getCoordinates().getLatitude());
			
			int updatedColumns = pstmt.executeUpdate();
			
			if(updatedColumns <= 0){
				System.out.println("NO COLUMS UPDATED");
				return -1;
			}
			
			resSet = pstmt.getGeneratedKeys();
			if(resSet.next()){
				return resSet.getInt(1); //Returns the generated dockingstationID from db
			}
			
			
		}catch(SQLException e){
			e.printStackTrace();
			System.out.println("EXCEPTION");
			return -1;
		}finally{
			DatabaseConnection.close(resSet);
		}
		System.out.println("END");
		return -1;
	}
	
	/**
	 * Deletes selected dockingstation from database.
	 * @param dockID is the ID number of the dockingstation to be deleted.
	 * @return 1 if successful and -1 if not.
	 */
	
	public int deleteDockingStation(int dockID){
		String sql = "DELETE FROM Dockingstation WHERE dock_id = ?";
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement pstmt = con.prepareStatement(sql)
			){
			
			pstmt.setInt(1, dockID);
			
			int updatedColumns = pstmt.executeUpdate();
			
			if(updatedColumns <= 0){
				return -1;
			}
			return updatedColumns;
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return -1;
	}
	
	/**
	 * Method for retrieving a docking station based on its unique ID number..
	 *
	 * @param dockID the unique id for a docking station.
	 * @return object of type DockingStation if successfull and null if not.
	 */
	public DockingStation getDockingStation(int dockID){
		String sqlDock = "SELECT * FROM Dockingstation WHERE dock_id = ?";
		ResultSet resultSetDock = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement pstmtDock = con.prepareStatement(sqlDock)
			){
			
			if(!dockingStationIDExists(dockID)) return null;
			
			pstmtDock.setInt(1, dockID);
			resultSetDock = pstmtDock.executeQuery();

			if(resultSetDock.next()){
				return new DockingStation
						(dockID, resultSetDock.getString("name"), getCapacitySQL(dockID),
								new Coordinate(resultSetDock.getDouble("dock_longitude"), resultSetDock.getDouble("dock_latitude")),
										resultSetDock.getInt("power_used"));
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resultSetDock);
		}
		return null;
	}
	
	/**
	 * Executes edits to Dockingstation tabel.
	 *
	 * @param args A list of arguments in the correct order. Ie.: editDockingStation(1, "Some name", 12.3456, 65.4321);
	 * @return dock_id if the update was successful and -1 if something went wrong.
	*/
	public int editDockingStation(Object... args){
		
		String selectDock = "SELECT * FROM Dockingstation WHERE dock_id = ?";
		
		final int MAX_MIN_LIMIT = 30;
		int dockid = 0;
		Double longitude = 0.0;
		Double latitude = 0.0;
		String name = null;
		
		for(int i = 0; i < args.length; i++){
			if(args[i] instanceof Integer && dockid == 0){
				dockid = (Integer) args[i];
			}
			else if(args[i] instanceof Double && (Double) args[i] < MAX_MIN_LIMIT){
				longitude = (Double) args[i];
			}
			else if(args[i] instanceof Double && (Double) args[i] > MAX_MIN_LIMIT){
				latitude = (Double) args[i];
			}
			else if(args[i] instanceof String && name == null){
				name = (String) args[i];
			}
		}
		
		if(dockid < 0) return -1;
		
		ResultSet rsDock = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()){
			
			try(PreparedStatement getDock = con.prepareStatement(selectDock, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE)){
				con.setAutoCommit(false);
				getDock.setInt(1, dockid);
				rsDock = getDock.executeQuery();
				
				
				if(rsDock.next()){
					if(name != null){
						rsDock.updateString("name", name);
					}
					if(longitude != 0){
						rsDock.updateDouble("dock_longitude", longitude);
					}
					if(latitude != 0){
						rsDock.updateDouble("dock_latitude", latitude);
					}
					rsDock.updateRow();
				}
				con.commit();
				
				
			}catch(SQLException e){
				e.printStackTrace();
				con.rollback();
			}finally{
				con.setAutoCommit(true);
			}
			return dockid;
			
		}catch(SQLException e){
			e.printStackTrace();
			return -1;
		}finally{
			DatabaseConnection.close(rsDock);
		}
		
	}
	
	/**
	 * Retrives all registered dockingstations from database.
	 * @return a arraylist of dockingstations.
	 */
	public ArrayList<DockingStation> getAllDockingStations(){
		String sql = "SELECT * FROM Dockingstation";
		ResultSet resultSet = null;
		ArrayList<DockingStation> dockingStations = new ArrayList<>();
		
		try(Connection con = DatabaseConnection.getInstance().getConnection()
			){
			try(PreparedStatement pstmt = con.prepareStatement(sql)){
				con.setReadOnly(true);
				resultSet = pstmt.executeQuery();

				//NOTE: Removed get all bicycles, see reasoning in getDockingstation
				SlotDAO slotDAO = new SlotDAO();
				DockingStation ds;
				while(resultSet.next()){
					ds = new DockingStation(resultSet.getInt("dock_id"), resultSet.getString("name"),
							getCapacitySQL(resultSet.getInt("dock_id")), new Coordinate(resultSet.getDouble("dock_latitude"),
							resultSet.getDouble("dock_longitude")), resultSet.getInt("power_used"));
					ds.setCapacity(slotDAO.getDockingstationCapacity(ds.getDockingID()));
					dockingStations.add(ds);
				}
				return dockingStations;
				
			}catch(SQLException e){
				e.printStackTrace();
			}finally{
				con.setReadOnly(false);
			}
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resultSet);
		}
		
		return null;
	}
	
	/**
	 * Finds the capacity of a selected dockingstation.
	 * @param dockid from the selected dockingstation.
	 * @return an int representing the capacity.
	 */
	public int getCapacitySQL(int dockid){
		String sqlXBikes = "SELECT count(slot_id) FROM Slot WHERE dock_id =?";
		ResultSet resultSet = null;
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement pstmtXBikes = con.prepareStatement(sqlXBikes)){
			
			pstmtXBikes.setInt(1, dockid);
			resultSet = pstmtXBikes.executeQuery();
			int capacity;
			if(resultSet.next()){
				capacity = resultSet.getInt(1);
				return capacity;
			}else{
				return -1;
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}finally{
			DatabaseConnection.close(resultSet);
		}
		return -1;
	}
	
	public String getName(DockingStation dockingStation){
		return dockingStation.getName();
	}
	
	public Double getLatitude(DockingStation dockingStation){
		return dockingStation.getCoordinates().getLatitude();
	}
	
	public Double getLongitude(DockingStation dockingStation){
		return dockingStation.getCoordinates().getLongitude();
	}
	
	public int getPowerUsed(DockingStation dockingStation){
		return dockingStation.getPowerUsage();
	}
	
	/**
	 * Retrieves all bicycles docked at a selected dockingstation.
	 * @param dockID from selected dockingstation.
	 * @return an arraylist containing Bicycle objects.
	 */
	
	public ArrayList<Bicycle> getBicyclesInDockingStation(int dockID){
		String sql = "SELECT * FROM Bicycle LEFT JOIN Slot ON Slot.bike_id = Bicycle.bike_id WHERE Slot.dock_id = ?";
		
		ResultSet resultSet = null;
		
		try(Connection con = DatabaseConnection.getInstance().getConnection();
			PreparedStatement pstatement = con.prepareStatement(sql)){
			
			ArrayList<Bicycle> bikes = new ArrayList<>();
			CoordinateDAO coordDAO = new CoordinateDAO();
			Coordinate coordinate;
			TypeDAO typeDAO = new TypeDAO();
			Type type;
			Bicycle bike;
			
			pstatement.setInt(1, dockID);
			resultSet = pstatement.executeQuery();
			
			while(resultSet.next()){
				bike = new Bicycle();
				type = typeDAO.getType(resultSet.getInt("typeID"));
				bike.setBicycleID(resultSet.getInt("bike_id"));
				bike.setChargingLvl(resultSet.getInt("battery_percent"));
				bike.setPrice(type.getPrice());
				bike.setMake(resultSet.getString("make"));
				bike.setDistanceTravelled(resultSet.getDouble("distanceTravelled"));
				bike.setTripCount(resultSet.getInt("trips"));
				bike.setDateRegistered(resultSet.getDate("registered_date").toLocalDate());
				bike.setStatus(resultSet.getString("status"));
				coordinate = coordDAO.getCoordinates(bike.getBicycleID());
				bike.setCoordinates(coordinate);
				bikes.add(bike);
			}
			
			return bikes;
			
		}catch(SQLException e){
			e.printStackTrace();
			return null;
		}finally{
			DatabaseConnection.close(resultSet);
		}

	}
	
	/**
	 * A method which checks if a docking ID already exists or not.
	 * @param dockId from selected dockingstation.
	 * @return true if it already exists and false if not.
	 */
	private boolean dockingStationIDExists(int dockId){
		ArrayList<DockingStation> ds = getAllDockingStations();
		
		for(DockingStation curr : ds){
			if(dockId == curr.getDockingID()) return true;
		}
		return false;
	}
	
}
