package db;

import data.Bicycle;
import data.DockingStation;

import java.sql.*;

public class SlotDAO {
    private PreparedStatement ps;
    private ResultSet rs;
    private Statement stmt;


    public int addSlot(int dockID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("INSERT INTO Slot (dock_id) VALUES (?)", Statement.RETURN_GENERATED_KEYS);
            ps.setInt(1,dockID);
            int update = ps.executeUpdate();

            if(update == 0){
                return -1;
            }
            rs = ps.getGeneratedKeys();
            if(rs.next()){
                return rs.getInt(1);
            }
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int getDockingstationCapacity(int dockID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT COUNT(dock_id) FROM Slot WHERE dock_id = ?");
            ps.setInt(1,dockID);
            rs = ps.executeQuery();

            int ret = 0;
            if(rs.next()){
                return rs.getInt(1);
            }


        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }



    public int getEmptySlot(int dockID){
       try(Connection con = DatabaseConnection.getInstance().getConnection()){
           ps = con.prepareStatement("SELECT slot_id FROM Slot WHERE dock_id=? AND bike_id IS NULL LIMIT 1");
           ps.setInt(1, dockID);

           rs = ps.executeQuery();
           if(rs.next()) return rs.getInt(1);

       }catch(SQLException e){
           e.printStackTrace();
       }finally {
           DatabaseConnection.close(ps);
           DatabaseConnection.close(rs);
       }
       return -1;
    }

    public int addBicycleToSlot(Bicycle bike, DockingStation doc) {
        try (Connection con = DatabaseConnection.getInstance().getConnection()) {
            ps = con.prepareStatement("UPDATE Slot SET bike_id=? WHERE dock_id=? AND slot_id=?");
            if (getEmptySlot(doc.getDockingID()) < 0) return -1;
            ps.setInt(1, bike.getBicycleID());
            ps.setInt(2, doc.getDockingID());
            ps.setInt(3, getEmptySlot(doc.getDockingID()));

            int update = ps.executeUpdate();
            if (update > 0) return update;

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int countSlots(){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            ps = con.prepareStatement("SELECT COUNT(slot_id) FROM Slot");
            rs = ps.executeQuery();

            if(rs.next()){
                return rs.getInt(1);
            }

            return -1;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
            DatabaseConnection.close(rs);
        }
        return -1;
    }

    public int rmBikeFromSlot(Bicycle b){
        try (Connection con = DatabaseConnection.getInstance().getConnection()) {
            ps = con.prepareStatement("UPDATE Slot SET bike_id=NULL WHERE bike_id=?");
            ps.setInt(1, b.getBicycleID());

            int update = ps.executeUpdate();
            if(update > 1) return update;
        }catch(SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }return -1;
    }


    @SuppressWarnings("Duplicates")
    public int deleteSlot(int dockID){
        try(Connection con = DatabaseConnection.getInstance().getConnection()){
            int slotID = 0;
            ps = con.prepareStatement("delete from Slot where bike_id IS NULL and dock_id = ? limit 1");
            ps.setInt(1,dockID);
            int update = ps.executeUpdate();

            return update;
        }catch (SQLException e){
            e.printStackTrace();
        }finally {
            DatabaseConnection.close(ps);
        }
        return -1;
    }
}
