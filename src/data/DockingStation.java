package data;

/**
 * This class is the data class for all dockingstations. Talks to database
 * through the
 * @see db.DockingStationDAO
 */

public class DockingStation {
    private int dockingID;
    private String name;
    private int capacity;
    private Coordinate coordinates;
    private int powerUsage;
    

    public DockingStation(int dockingID, String name, int capacity, Coordinate coordinates, int powerUsage) {
        this.dockingID = dockingID;
        this.name = name;
        this.capacity = capacity;
        this.coordinates = coordinates;
        this.powerUsage = powerUsage;
    }

    public DockingStation(String name, Coordinate coordinates){
        this.name = name;
        this.coordinates = coordinates;
    }

    public DockingStation(){}

    public String getName() {
        return name;
    }

    public int getDockingID() {
        return dockingID;
    }
    
    public void setPowerUsage(int power){
        powerUsage += power;
    }
    
    public int getPowerUsage(){
        return powerUsage;
    }
    
    public int getCapacity(){
        return capacity;
    }
    
    public Coordinate getCoordinates(){
        return coordinates;
    }
    
    public void setDockingID(int ID){
        dockingID = ID;
    }
    
    public void setName(String namee){
        name = namee;
    }
    
    public void setCapacity(int cap){
        capacity = cap;
    }
    
    public void setCoordinates(Coordinate coord){
        coordinates = coord;
    }

    @Override
    public String toString() {
        return name;
    }
    
    public String myToString(){
        return name + ". Coords: " + coordinates.getLatitude() + " , " + coordinates.getLongitude()
                + " ID: " + dockingID + ". Slots: " + capacity;
    }
    
    //Returns true if they are the same
    @Override
    public boolean equals(Object obj) {
        boolean result = false;
    
        if(!(obj instanceof DockingStation)){
            return false;
        }else {
            DockingStation dock = (DockingStation) obj;
            if(this.dockingID == dock.getDockingID()){
                result = true;
            }
        }
        return result;
    }
    
    @Override
    public int hashCode(){
        return dockingID;
    }
    
    
}

