package data;

/**
 * This class is the data class for all administrators. Talks to database
 * through the
 * @see db.AdministratorDAO
 */

public class Administrator {
    private String email;
    private int id;

    public Administrator(String email, int id){
        this.email = email;
        this.id = id;
    }

    public Administrator(String email){
        this.email = email;
    }
    
    public Administrator(){
    }
    
    public void setEmail(String email){
        this.email = email;
    }
    
    public String getEmail(){
        return email;
    }
    
    public void setId(int id){
        this.id = id;
    }
    
    public int getId(){
        return id;
    }
}
