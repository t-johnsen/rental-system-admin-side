package data;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 * This class is the data class for all bicycles. Talks to database
 * through the
 * @see db.BicycleDAO
 */

public class Bicycle {

    public enum BikeStatus{
        docked,
        rented,
        repair
    }

    private BikeStatus bikeStatus;

    public void setBiiikesStatus(BikeStatus status){
        bikeStatus = status;
    }

    public BikeStatus getBiiikesStatus(){
        return bikeStatus;
    }

    private int bicycleID;
    private int chargingLvl;
    private Coordinate coordinates;
    private String status;
    private int price;
    private String make;
    private Type typeObj;
    private String type;
    private double distanceTravelled;
    private int tripCount;
    private LocalDate dateRegistered;
    private int toBeDockedIn;
    private int isDockedIn;
    private int stepCount;
    private int simulatedTimeDocked;
    private double distanceToBeTravelled;
    private boolean hasSlot;
    private int timeRepair;
    private DockingStation destinationDocking;
    private Coordinate[] routeCoordinates;



    public Bicycle(String make){
        this.make = make;
        dateRegistered = LocalDate.now();
    }

    public Bicycle(String make, LocalDate date){
        this.make = make;
        this.dateRegistered = date;
    }
    
    public Bicycle(){}

    public Bicycle(int bicycleID, int chargingLvl, Coordinate coordinates, String status, int price, String make, Type type, double distanceTravelled, int tripCount, LocalDate dateRegistered) {
        this.bicycleID = bicycleID;
        this.chargingLvl = chargingLvl;
        this.coordinates = coordinates;
        this.status = status;
        this.price = price;
        this.make = make;
        this.typeObj = type;
        this.distanceTravelled = distanceTravelled;
        this.tripCount = tripCount;
        this.dateRegistered = dateRegistered;
    }

    public Bicycle(int bicycleID, String status, Coordinate coordinates, int chargingLvl) {
        this.bicycleID =bicycleID;
        this.status = status;
        this.coordinates =coordinates;
        this.chargingLvl = chargingLvl;
    }

    /**
     * @return current price
     */
    public int getPrice(){
        return price;
    }

    /**
     * @return current make
     */
    public String getMake(){
        return make;
    }

    /**
     * @return current type as a Type-object
     */
    public Type getTypeObj() {
        return typeObj;
    }

    /**
     * @return current type as a string
     */
    public String getType(){return type;}

    /**
     * @return current status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @return current distance travelled
     */
    public double getDistanceTravelled(){ return distanceTravelled;}

    /**
     * @return current battery percentage
     */
    public int getChargingLvl() {
        return chargingLvl;
    }

    /**
     * @return bicycle ID
     */
    public int getBicycleID() {
        return bicycleID;
    }

    /**
     * @return date registered as a LocalDate-object
     */
    public LocalDate getDate(){
        return dateRegistered;
    }

    /**
     * @return current coordinates as a Coordinate-object
     */
    public Coordinate getCoordinates() {
        return coordinates;
    }

    /**
     * @return total trip count
     */
    public int getTripCount(){
        return tripCount;
    }

    /**
     * Sets new status for the bicycle
     * @param newStatus new status
     */
    public void setStatus(String newStatus) {
        status = newStatus;
    }




    /**
     * Sets a new Bicycle ID
     * @param bikeID new ID
     */
    public void setBicycleID(int bikeID){bicycleID=bikeID;}

    /**
     * Sets a new charging level for the bicycle
     * @param chrLvl new charging level
     */
    public void setChargingLvl(int chrLvl){chargingLvl=chrLvl;}

    /**
     * Sets a new price for the bicycle
     * @param priceIn new price
     */
    public void setPrice(int priceIn){price = priceIn;}

    /**
     * Sets a new make
     * @param makeIn new make
     */
    public void setMake(String makeIn){make = makeIn;}

    /**
     * Sets a new type
     * @param typeIn new type
     */
    public void setType(String typeIn){
        type=typeIn;
    }

    /**
     * Sets distance travelled
     * @param distTrav new distance travelled
     */
    public void setDistanceTravelled(Double distTrav){
        distanceTravelled = distTrav;
    }

    /**
     * Increments tripcount by given amount
     * @param trip trips to add
     */
    public void incrementTripCount(int trip){tripCount+=trip;}

    /**
     * Sets trip count
     * @param trip new trip count
     */
    public void setTripCount(int trip){
        tripCount = trip;
    }

    /**
     * Sets a new date registered
     * @param dateIn new date
     */
    public void setDateRegistered(LocalDate dateIn){dateRegistered=dateIn;}
    
    public void setCoordinates(Coordinate coord){
        coordinates = coord;
    }

    @Override
    public String toString() {
        return "ID: " + bicycleID; // + " Charge: " + chargingLvl + " Status: " + status + " Make: " + make + " Type: " + type + " Price: " + price + " Date Registerd: " + dateRegistered + " Location: " + coordinates;
    }

    public ArrayList<Integer> getBikeIDs(ArrayList<Bicycle> bikes){
        ArrayList<Integer> bikeIDs = new ArrayList<>();
        for (Bicycle bike:bikes){
            bikeIDs.add(bike.getBicycleID());
        }
        return bikeIDs;
    }

    /**
     * @return registered date as a string
     */
    public String getDateString(){
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd LLLL yyyy");
        String formattedString = dateRegistered.format(formatter);
        return formattedString;
    }

    @Override
    public boolean equals(Object obj) {
        boolean result = false;

        if(!(obj instanceof Bicycle)){
            return false;
        }else {
            Bicycle bike = (Bicycle) obj;
            if(this.getBicycleID() == bike.getBicycleID()){
                result = true;
            }
        }
        return result;
    }

    @Override
    public int hashCode(){
        return bicycleID;
    }

    /*****FOLLOWING METHODS ARE FOR SIMULATION PURPOSES******/

    public void setToBeDockedInID(int dockID){
        toBeDockedIn = dockID;
    }
    public int getToBeDockedInID(){
        return toBeDockedIn;
    }

    public void setIsDockedIn(int dockid){
        isDockedIn = dockid;
    }
    public int getIsDockedIn(){
        return isDockedIn;
    }
    public void setStepCount(int steps){
        stepCount = steps;
    }
    public void increaseStepCount(){
        stepCount++;
    }
    public int getStepCount(){
        return stepCount;
    }
    public void setRouteCoordinates(Coordinate[] route){
        routeCoordinates = route;
    }
    public Coordinate[] getRouteCoordinates(){
        return routeCoordinates;
    }
    public void setDestinationDocking(DockingStation dock){
        destinationDocking = dock;
    }
    public DockingStation getDestinationDocking(){
        return destinationDocking;
    }
    public void increaseTripByOne(){
        tripCount++;
    }
    public void increaseChargingLevel(){
        chargingLvl+=5; //Ask if this is enough or needs more
    }
    public void decreaseChargingLevel(){
        chargingLvl--;
    }
    public void setSimulatedDockTime(int time){
        simulatedTimeDocked = time;
    }
    public void decreaseTimeDocked(){
        simulatedTimeDocked--;
    }
    public int getSimulatedTimeDocked(){
        return simulatedTimeDocked;
    }
    public void setDistanceToBeTravelled(double km){
        distanceToBeTravelled += km;
    }
    public double getDistanceToBeTravelled(){
        return distanceToBeTravelled;
    }
    public void increaseDistanceTravelled(double km){
        distanceTravelled+= km;
    }
    public void decreaseTimeRepair(){
     timeRepair--;
    }
    public void setTimeRepair(int time){
     timeRepair = time;
    }
    public int getTimeRepair(){
        return timeRepair;
    }
    public boolean getHasSlot(){
        return hasSlot;
    }
    public void setHasSlot(boolean bool){
        hasSlot = bool;
    }
}
