package data;

import java.time.LocalDate;

/**
 * This class is the data class for all repairs. Talks to database
 * through the
 * @see db.RepairDAO
 */

public class Reparation {
    private int repairID;
    private LocalDate dateSent;
    private LocalDate dateReturned;
    private String repairReqDesc;
    private double price;
    private String repairDesc;

    public Reparation(){};

    public Reparation(String repairReqDesc){
        this.repairReqDesc = repairReqDesc;
    }

    public Reparation(LocalDate dateSent, String repairReqDesc){
        this.dateSent = dateSent;
        this.repairReqDesc = repairReqDesc;
    }

    public Reparation(int repairID,LocalDate dateSent, LocalDate dateReturned, String repairReqDesc, int price, String repairDesc){
        this.repairID = repairID;
        this.dateSent = dateSent;
        this.dateReturned = dateReturned;
        this.repairReqDesc = repairReqDesc;
        this.price = price;
        this.repairDesc = repairDesc;
    }

    public void setRepairID(int repairID){ this.repairID = repairID;}

    public int getRepairID() { return repairID;}

    public void setPrice(double price) {
        this.price = price;
    }

    public void setRepairdesc(String repairDesc) {
        this.repairDesc = repairDesc;
    }

    public void setDateReturned(LocalDate dateReturned) {
        this.dateReturned = dateReturned;
    }

    public void setDateSent(LocalDate dateSent){this.dateSent = dateSent;}

    public void setRepairReqDesc(String repairReqDesc) {this.repairReqDesc = repairReqDesc; }

    public double getPrice() {
        return price;
    }

    public LocalDate getDateReturned() {
        return dateReturned;
    }

    public LocalDate getDateSent() {
        return dateSent;
    }

    public String getRepairReqDesc() {
        return repairReqDesc;
    }

    public String getRepairdesc() {
        return repairDesc;
    }

    @Override
    public String toString() {
        return "Date sent: " + dateSent + "\t Date Returned: " + dateReturned + "\n Repairs required: " + repairReqDesc + "\n Repairs done: " + repairDesc + "\n Price: " + price + "\n";
    }
}
