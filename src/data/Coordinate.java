package data;

import java.time.*;

/**
 * This class is the data class for all coordinates. Talks to database
 * through the
 * @see db.CoordinateDAO
 */

public class Coordinate {
    private Double longitude;
    private Double latitude;
    private LocalDate date;
    private LocalTime time;

    public Coordinate(){}

    public Coordinate(Double latitude, Double longitude, LocalDate date, LocalTime time){
        this.latitude = latitude;
        this.longitude = longitude;
        this.date = date;
        this.time = time;
    }
    
    public Coordinate(Double latitude, Double longitude){
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public Double getLatitude() {
        return latitude;
    }
    public Double getLongitude() {
        return longitude;
    }
    public LocalDate getDate() { return date; }
    public LocalTime getTime() {return time; }

    public void setDate(LocalDate date) { this.date = date;}
    public void setLatitude(Double latitude) { this.latitude = latitude; }
    public void setLongitude(Double longitude) { this.longitude = longitude; }
    public void setTime(LocalTime time) { this.time = time; }

    @Override
    public String toString() {
       return "  Lat: " + latitude + " Long: " + longitude;
    }
}
