package data;

/**
 * This class is the data class for all types. Talks to database
 * through the
 * @see db.TypeDAO
 */

public class Type {
    private int typeID;
    private String type;
    private int price;

    public Type(){};


    public Type(String type, int price){
        this.type = type;
        this.price = price;
    }

    public Type(int typeID, String type, int price){
        this.typeID = typeID;
        this.type = type;
        this.price = price;
    }

    public int getPrice() {
        return price;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return type;
    }
}
