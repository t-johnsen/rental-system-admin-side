package util;

import db.AdministratorDAO;
import db.DatabaseConnection;

import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.PBEKeySpec;
import java.math.BigInteger;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.UUID;

/**
 * This class represents a manager over everything which has to do with
 * password and administrator changes. In example: adding new administrators,
 * changing password, generating temporary password, hashing and validating
 * administrators when logging in.
 */
public class PasswordManager{
	
	
	/**
	 * The method simply generates a temporary password random password.
	 * @return 		   A temporary password to use for first log in.
	 */
	public static String generateTempPassword(){
		String uuid =  UUID.randomUUID().toString();
		String[] pass = uuid.split("-");
		return pass[0];
	}
	
	/**
	 * Method which hashes password with salt and iterations ready to store in database.
	 * @param password Users password from input.
	 * @return		   Hashed password with salt and iterations.
	 */
	public static String hashPassword(String password){
		final int ITERATIONS = 1000;
		char[] charArray = password.toCharArray();
		byte[] salt = getSalt().getBytes();
		
		PBEKeySpec keySpec = new PBEKeySpec(charArray, salt, ITERATIONS, 64 * 8);
		SecretKeyFactory sKeyFactory;
		
		try{
			sKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			byte[] hash = sKeyFactory.generateSecret(keySpec).getEncoded();
			
			return ITERATIONS + ":" + toHexadecimal(salt) + ":" + toHexadecimal(hash);
			
		}catch(NoSuchAlgorithmException | InvalidKeySpecException e){
			e.printStackTrace();
		}
		return null;
	}
	
	/**
	 * This method takes two parameters and checks if the two hashed passwords are the same.
	 * @param inputPassword	 The input password from the user when logging in.
	 * @param storedPassword The stored hashed password in database.
	 * @return				 If password is valid method returns true. If not returns false.
	 */
	public static boolean validatePassword(String inputPassword, String storedPassword){
	
		if(inputPassword == null || storedPassword == null) return false;
		
		String[] passwordParts = storedPassword.split(":");
		final int ITERATIONS = Integer.parseInt(passwordParts[0]);
		byte[] salt = fromHexadecimal(passwordParts[1]);
		byte[] hash = fromHexadecimal(passwordParts[2]);
		
		PBEKeySpec keySpec = new PBEKeySpec(inputPassword.toCharArray(), salt, ITERATIONS, hash.length * 8);
		SecretKeyFactory sKeyFactory;
		
		try{
			sKeyFactory = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA1");
			
			byte[] theTestHash = sKeyFactory.generateSecret(keySpec).getEncoded();
			
			return slowEquals(theTestHash, hash);
			
		}catch(NoSuchAlgorithmException | InvalidKeySpecException e){
			e.printStackTrace();
		}
		return false;
	}
	
	/**
	 * Method which changes password of a logged in admin. Updates database.
	 * @param newPassword the new password from admin.
	 * @param oldPassword the old password from admin.
	 * @param admin 	  the current admin who is logged in.
	 * @return 			  true if password is changed successfully and false if not.
	 */
	public static boolean changePassword(String newPassword, String oldPassword, AdministratorDAO admin){
		int id = admin.getAdminId(); //TODO: May not work -> depends on login class login
		System.out.println("Admin id from changePassword: " + id);
		String sqlQ = "UPDATE Administrator SET hashedPassword = ? WHERE admin_id = ?";
		String old = admin.getHashFromDB(id);
		if(!validatePassword(oldPassword, old)){
			return false;
		}
		try(Connection con = DatabaseConnection.getInstance().getConnection()){
			try(PreparedStatement prepStmt = con.prepareStatement(sqlQ)){
				con.setAutoCommit(false);
				
				prepStmt.setString(1, hashPassword(newPassword));
				prepStmt.setInt(2, id);
				int rowsAffected = prepStmt.executeUpdate();
				con.commit();
				if(rowsAffected == 1){
					return true;
				}
				
			}catch(SQLException e){
				e.printStackTrace();
				con.rollback();
				return false;
			}finally{
				con.setAutoCommit(true);
			}
			
			
		}catch(SQLException e){
			e.printStackTrace();
			return false;
		}
		return false;
	}
	
	/**
	 * The method takes two hashed values and compare their equality.
	 * @param testHash The hash from inputPassword.
	 * @param hash	   The hash from storedPassword.
	 * @return		   If equal returns true. If not equal returns false.
	 */
	private static boolean slowEquals(byte[] testHash, byte[] hash){
		
		int diff = hash.length ^ testHash.length;
		for(int i = 0; i < hash.length && i < testHash.length; i++){
			diff |= hash[i] ^ testHash[i];
		}
		return diff == 0;
	}
	
	/**
	 * Known algorithm which converts a string of hexadecimal characters to an array of bytes.
	 * @param hex A string of hexadecimals.
	 * @return    The hex string decoded into a byte array.
	 */
	private static byte[] fromHexadecimal(String hex){
		byte[] bytes = new byte[hex.length() / 2];
		
		for (int i = 0; i < bytes.length; i++) {
			bytes[i] = (byte) Integer.parseInt(hex.substring(2 * i, 2 * i + 2), 16);
		}
		return bytes;
	}
	
	/**
	 * Known algorithm which converts an array of bytes to a hexadecimal string.
	 * @param bytes An array of bytes.
	 * @return 		A string of hexadecimals.
	 */
	private static String toHexadecimal(byte[] bytes){
		BigInteger bigInt = new BigInteger(1, bytes);
		
		String hexa = bigInt.toString(16);
		int paddingLength = (bytes.length * 2) - hexa.length();
		if(paddingLength > 0){
			return String.format("%0"+paddingLength+"d", 0) + hexa;
		}else {
			return hexa;
		}
	}
	
	/**
	 * Method to generate random salt.
	 * @return A string of salt.
	 */
	private static String getSalt(){
		try{
			SecureRandom secureRandom = SecureRandom.getInstance("SHA1PRNG");
			byte[] salt = new byte[16];
			secureRandom.nextBytes(salt);
			
			return salt.toString();
			
		}catch(NoSuchAlgorithmException e){
			e.printStackTrace();
		}
		return null;
	}
	
}
