package util;

import data.Administrator;
import db.AdministratorDAO;
import db.DatabaseConnection;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

import static db.AdministratorDAO.addAdministrator;
import static db.AdministratorDAO.getAdminId;

/**
 * This class works as a DAO-class and security level between the database,
 * graphical user interface and
 * @see PasswordManager class.
 * All login features and changes to administrator happens through this class.
 */

public class LogIn{
	
	private static AdministratorDAO admin;
	
	/**
	 * When logging in to program this method validates login information.
	 * @param username the users email address.
	 * @param password the users password.
	 * @return 		   true if login is successful and false if not.
	 */
	public static boolean logIn(String username, String password){
		boolean ok = false;
		int id = getAdminId(username);
		
		if(id >= 0){
			admin = new AdministratorDAO(new Administrator());
			if(PasswordManager.validatePassword(password, admin.getHashFromDB(id))){
				ok = true;
				admin.setAdminEmail(username);
				admin.setAdminId(id);
			}
		}
		return ok;
	}
	
	/**
	 * Adds a new admin to the database and calls method from
	 * @see AdministratorDAO
	 * @param username the new users email address.
	 * @return 		   true if new admin is registered and false if not.
	 */
	public static boolean newAdmin(String username){
		
		return addAdministrator(username);
	}
	
	/**
	 * Method which generates a random password to users who has forgotten
	 * their password before logging in.
	 * @param username the users email address.
	 * @return 		   true if new password is sent and false if not.
	 */
	public static boolean forgottenPassword(String username){
		boolean ok = false;
		int id = getAdminId(username);
		if(id < 0) return false;
		
		String password = PasswordManager.generateTempPassword();
		if(!AdministratorDAO.sendEmail(username, password)){
			return false;
		}
		
		String sqlQ = "UPDATE Administrator SET hashedPassword = ? WHERE admin_id = ?";
		try(Connection con = DatabaseConnection.getInstance().getConnection()
			){
			try(PreparedStatement pStmt = con.prepareStatement(sqlQ)){
				con.setAutoCommit(false);
				pStmt.setString(1, PasswordManager.hashPassword(password));
				pStmt.setInt(2, id);
				int affected = pStmt.executeUpdate();
				con.commit();
				if(affected > 0)
					ok = true;
			}catch(SQLException e){
				e.printStackTrace();
				con.rollback();
			}finally{
				con.setAutoCommit(true);
			}
			
		}catch(SQLException e){
			e.printStackTrace();
		}
		return ok;
	}
	
	/**
	 * Changes password for the user and updates database by calling method in
	 * @see PasswordManager
	 * @param oldPassword the old password from user.
	 * @param newPassword the new password from user.
	 * @return 			  true if password is reset successfully and false if not.
	 */
	public static boolean changePword(String oldPassword, String newPassword){
		return PasswordManager.changePassword(newPassword, oldPassword, admin);
	}
	
	/**
	 * @return the current admin which is logged in to the program.
	 */
	public static AdministratorDAO getCurrAdmin(){
		return admin;
	}
}
