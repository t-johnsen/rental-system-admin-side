# Trondheim bicycle project

## Links

* [Timesheet](https://docs.google.com/spreadsheets/d/1idBlO-70XIlTrWi4eIKFH-OfRs_U8G4zdOMmO6q5OxA/edit#gid=1646105146)
* [Project plan](https://docs.google.com/spreadsheets/d/1TPOrgSjlg-Tmbbg6tbSAxkRaRwzetSs4TH0eEqfKm1E/edit#gid=1591252235)

## Collaboration agreement

* [Collaboration agreement](https://docs.google.com/document/d/1z0lweNfLoGzPWfOK37ZzRVVf6C-pUIcJf8XUvhiihOs/edit)

## Commits

* Always comment the changes done
* Never commit directly to master, always use development-branch
* Never add files to master unless 100% sure it's errorproof
* 

## Timetracking

* The date, hourcount and a short description of what's been done 
should be added immediately after every working-session

## Authors (sorted alphabetically)

* **Ådne Eide Stavseng**
* **Mari Teiler-Johnsen**
* **Mona Ullah**
* **Brage Wichstrøm**